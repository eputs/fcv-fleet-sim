FCV-Fleet-Sim
=============

This program contains a Fuel Cell Vehicle (FCV) model which can be used for planning FCV fleets. It has both technical and economic models of the various model components. We plan to eventually merge this model into our main software, [EV-Fleet-Sim](https://gitlab.com/eputs/ev-fleet-sim).

Installation:
-------------

Clone the repository to your computer, and open a terminal in the repository's root folder.

### User installation:

```bash
pip install .
```

### Developer installation:

Linux/MacOS:

```bash
pip install --prefix ~/.local/ -e .
```

Windows:

```bash
pip install -e .
```

Usage
-----

The usage instructions are quite similar to those of EV-Fleet-Sim. If you are stuck, your best bet is to read through [EV fleet-sim's documentation](https://ev-fleet-sim.online/docs/usage.html), and to apply it to this software. We recommend that you [contact us](https://ev-fleet-sim.online/contact.html) if you are seriously considering to use this model.