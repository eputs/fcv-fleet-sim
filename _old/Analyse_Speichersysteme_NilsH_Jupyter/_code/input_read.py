import numpy as np
import scipy.io as sio
import scipy as sp
from scipy.interpolate import interp1d
from scipy.ndimage.interpolation import shift

import os.path
from os import path
import pathlib

def get_input_directory():
    directory='input'
    current_directory=str(pathlib.Path().absolute())
    input_directory=current_directory+directory
    i=0
    while not path.exists(input_directory):
        #print(input_directory)
        if i<10:
            i=i+1
        else:
            input_directory='Ordner wurde nicht gefunden'
            print(input_directory)
            break
        current_directory_new = current_directory[0:current_directory.rfind(os.sep)]
        input_directory=current_directory_new+'/'+directory
        current_directory = current_directory_new
    input_directory=input_directory+'/'
    return input_directory


def HTW_profiles():
    input_directory=get_input_directory()
    #Einlesen der repräsentativen Lastprofile (insgesamt 74)
    mat_loadrofiles = sio.loadmat(input_directory+'MAT_74_Loadprofiles_1min_W_var.mat')
    #Daten liegen als dreiphasiges Drehstromsystem vor Phasenleistungen werden für diese Arbeit addiert
    return mat_loadrofiles['PL1']+mat_loadrofiles['PL2']+mat_loadrofiles['PL3']

def solar_data():
    input_directory=get_input_directory()
    #Einlesen der Solardaten
    csv = np.genfromtxt (input_directory+'sonneneinstrahlung_stuttgart_2019.txt', delimiter=";")
    FG_LBERG = csv[1:,5]
    FD_LBERG = csv[1:,4]
    z_h= csv[1:,7]
    ##Zenit
    #upsampling mit spline Interpolation
    f = interp1d(np.arange(0,np.size(z_h)), z_h, kind ='cubic')
    z = f(np.arange(0,np.size(z_h)-1,1/60))
    #Messwerte gelten genau genommen für Mitte des Messintervalls von einer Stunde deshalb Offset 30min einstellen
    z=shift(z, 30, cval=0)
    ##Solarstrahlung
    #Fehler sind durch Wert -999 dargestellt; wird hier für das Modell durch letzten gültigen Wert ersetzt            
    for i, value in enumerate(FG_LBERG):
        if value<0:
            FG_LBERG[i] = FG_LBERG[i-1]
    for i, value in enumerate(FD_LBERG):
        if value<0:
            FD_LBERG[i] = FD_LBERG[i-1]
    #Umrechnung der Messwerte von J/cm² Stündliche Einstrahlung zur Bestrahlungsstärke in W/m²
    G_h_h = (FG_LBERG+FD_LBERG)*10000/60/60
    ##Bestrahlungsstärke
    #upsampling mit spline Interpolation
    f = interp1d(np.arange(0,np.size(G_h_h)), G_h_h, kind ='cubic')
    G_h = f(np.arange(0,np.size(G_h_h)-1,1/60))
    #durch splines entstehen negtive Werte. Diese dürfen nicht vorkommen und werden durch 0 ersetzt, da es die späteren Berechnungen erleichtert
    for i, value in enumerate(G_h):
        if value<0:
            G_h[i] = 0
    #Messwerte gelten genau genommen für Mitte des Messintervalls von eienr Stunde deshalb Offset 30min kompensieren
    G_h=shift(G_h, 30, cval=0)
    return G_h, z


def H0():
    input_directory=get_input_directory()
    #Einlesen der Solardaten
    csv = np.genfromtxt (input_directory+'ho_profil_dynamisiert.csv', delimiter=";")
    x=[]
    h0_15min=np.array(x)
    csv_bottom=csv[0:214]
    csv_top=csv[214:365]
    for value in csv_top:
        h0_15min=np.append(h0_15min,value)
    for value in csv_bottom:
        h0_15min=np.append(h0_15min,value)

    #vis.plot(h0_15min[0:300])

    ##Bestrahlungsstärke
    #upsampling mit spline Interpolation
    f = interp1d(np.arange(0,np.size(h0_15min)), h0_15min, kind ='cubic')
    h0_1min = f(np.arange(0,np.size(h0_15min)-1,1/15))
    #durch splines entstehen negtive Werte. Diese dürfen nicht vorkommen und werden durch 0 ersetzt, da es die späteren Berechnungen erleichtert
    for i, value in enumerate(h0_1min):
        if value<0:
            h0_1min[i] = 0
    #Werte gelten genau genommen für Mitte des Intervalls von 15min deshalb Offset 7min kompensieren
    h0_1min=shift(h0_1min, 7, cval=0)
    h0_1000kWh=np.append(h0_1min,np.zeros(15))
    
    return h0_1000kWh