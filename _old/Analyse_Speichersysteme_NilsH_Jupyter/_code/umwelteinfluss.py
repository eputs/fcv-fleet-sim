import numpy as np
import visualisation as vis
import matplotlib.pyplot as plt

def vergleich(E_def,strommix_co2,batterie_co2,wasserstoffspeicher_co2,wasserstoff_erdgasdampfreformierung_co2,E_batterie_max_array,E_batterie_netto,P_elektrolyse_max_array,E_h2_netto,P_elektrolyse_max_array_wasserstoffverkauf,E_h2_verkauf):
    element=0
    years = np.arange(0,21)
    
    CO2_ohneSpeicher = years*E_def*strommix_co2
    CO2_ohneSpeicher = CO2_ohneSpeicher/1000 #in Tonnen
    ax=vis.plot(years, CO2_ohneSpeicher,'Nutzungsdauer in Jahren','CO2 Ausstoß in t','CO2-Emissionen über Nutzungsdauer von 20 Jahren')
    legend=['ohne Speicher']

    #batterie über 20 jahre bei 5kWh Kapazität
    for i,value in enumerate(E_batterie_max_array):
        if value>5:
            element=i
            break
    CO2_batterie = (E_def-E_batterie_netto[element])*strommix_co2*years+batterie_co2*E_batterie_max_array[element]
    CO2_batterie = CO2_batterie/1000
    vis.addtoplot(ax,years,CO2_batterie)
    legend.append('Batteriespeicher 5kWh')
    
    #Wasserstoffrückverstromung 1kW
    for i,value in enumerate(P_elektrolyse_max_array):
        if value>1000:
            element=i
            break
    CO2_h2_2kW = (E_def-E_h2_netto[element])*strommix_co2*years+wasserstoffspeicher_co2*P_elektrolyse_max_array[element]/1000
    CO2_h2_2kW=CO2_h2_2kW/1000
    vis.addtoplot(ax,years,CO2_h2_2kW)
    legend.append('Wasserstoffspeicher 1kW')

    #Wasserstoffverkauf 1kW
    for i,value in enumerate(P_elektrolyse_max_array_wasserstoffverkauf):
        if value>1000:
            element=i
            break
    CO2_h2_verkauf_2kW = years*E_def*strommix_co2+wasserstoffspeicher_co2*P_elektrolyse_max_array_wasserstoffverkauf[element]/1000-wasserstoff_erdgasdampfreformierung_co2*E_h2_verkauf[element]*years 
    CO2_h2_verkauf_2kW=CO2_h2_verkauf_2kW/1000
    vis.addtoplot(ax,years,CO2_h2_verkauf_2kW)
    legend.append('Wasserstoffverkauf 1kW')
    
    plt.legend(legend, fontsize=14)
    return ax