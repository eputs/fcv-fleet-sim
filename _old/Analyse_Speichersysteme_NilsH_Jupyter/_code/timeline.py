from datetime import datetime
import numpy as np

def minute_period(starttime_in, endtime_in):
    ##Sampletime
    #Array t mit 1 Minute Auflösung im Format yyyy-mm-dd hh:mm
    starttime = np.datetime64(starttime_in)
    endtime = np.datetime64(endtime_in)
    #starttime = np.datetime64('2019-02-25')
    #endtime = np.datetime64('2019-02-27')
    t=np.arange(starttime, endtime, dtype='datetime64[m]')
    
    return t

def day_period(starttime_in, endtime_in):
    ##Sampletime
    #Array t mit 1 Minute Auflösung im Format yyyy-mm-dd hh:mm
    starttime = np.datetime64(starttime_in)
    endtime = np.datetime64(endtime_in)
    #starttime = np.datetime64('2019-02-25')
    #endtime = np.datetime64('2019-02-27')
    t=np.arange(starttime, endtime, dtype='datetime64[D]')
    
    return t