import numpy as np
import math
from own_utility_functions import *

def allgemein_lastprofile(P_dif, P_einspeicher_max, P_ausspeicher_max, eff_einspeicher, E_speicher_max):
    size=np.size(P_dif)
    E_speicher_array = np.zeros(size)
    P_einspeicher_array = np.zeros(size)
    P_ausspeicher_array = np.zeros(size)
    for i,value in enumerate(P_dif):
        if value>0:
            P_ausspeicher=0
            if value<=P_einspeicher_max:
                P_einspeicher=value
            else:
                P_einspeicher=P_einspeicher_max
        else:
            P_einspeicher=0 
            if -value<=P_ausspeicher_max:
                P_ausspeicher=-value
            else:
                P_ausspeicher=P_ausspeicher_max
        if i>0: #Integrieren startet bei Index 1
            E_speicher_new = E_speicher_array[i-1]+(P_einspeicher*eff_einspeicher-P_ausspeicher/eff_ausspeicher)/60/1000
            if E_speicher_new<=E_speicher_max and E_speicher_new>=0:
                E_speicher_array[i]=E_speicher_new
            else:
                E_speicher_array[i]=E_speicher_array[i-1]
                P_einspeicher=0
                P_ausspeicher=0
        P_einspeicher_array[i] = P_einspeicher
        P_ausspeicher_array[i] = P_ausspeicher
    return E_speicher_array, P_einspeicher_array, P_ausspeicher_array


def hybrid(P_dif, P_elektrolyse_max, P_fuelcell_max, P_battery_charge_max, P_battery_discharge_max, eff_elektrolyse, eff_fuelcell, eff_battery, E_h2_max, E_battery_max, E_h2_start):
    E_h2, E_bat, P_elektrolyse_array, P_fuelcell_array, P_battery_charge_array, P_battery_discharge_array=hybrid_lastprofile(P_dif, P_elektrolyse_max, P_fuelcell_max, P_battery_charge_max, P_battery_discharge_max, eff_elektrolyse, eff_fuelcell, eff_battery, E_h2_max, E_battery_max, E_h2_start)
    return E_h2, E_bat
   
def hybrid_lastprofile(P_dif, P_elektrolyse_max, P_fuelcell_max, P_battery_charge_max, P_battery_discharge_max, eff_elektrolyse, eff_fuelcell, eff_battery, E_h2_max, E_battery_max, E_h2_start):
    size=np.size(P_dif)
    E_h2 = np.zeros(size)
    E_h2[0] = E_h2_start
    E_bat = np.zeros(size)
    P_elektrolyse_array = np.zeros(size)
    P_battery_charge_array = np.zeros(size)
    P_fuelcell_array = np.zeros(size)
    P_battery_discharge_array = np.zeros(size)
    
    for i,value in enumerate(P_dif):
        if value>0:
            P_fuelcell=0
            P_battery_discharge=0
            if value<=P_elektrolyse_max:
                P_elektrolyse=value
                P_battery_charge=0
            else: 
                if value<=P_elektrolyse_max+P_battery_charge_max:
                    P_elektrolyse=P_elektrolyse_max
                    P_battery_charge=value-P_elektrolyse_max
                else:
                    P_elektrolyse=P_elektrolyse_max
                    P_battery_charge=P_battery_charge_max
        else:
            P_elektrolyse=0
            P_battery_charge=0
            if -value<=P_fuelcell_max:
                P_fuelcell=-value
                P_battery_discharge=0
            else: 
                if -value<=P_fuelcell_max+P_battery_discharge_max:
                    P_fuelcell=P_fuelcell_max
                    P_battery_discharge=-value-P_fuelcell_max
                else:
                    P_fuelcell=P_fuelcell_max
                    P_battery_discharge=P_battery_discharge_max
        
        if i>0:
            E_h2_new = E_h2[i-1]+(P_elektrolyse*eff_elektrolyse-P_fuelcell/eff_fuelcell)/60/1000
            if E_h2_new<=E_h2_max and E_h2_new>=0:
                E_h2[i]=E_h2_new
            else:
                if E_h2_new>1:
                    P_elektrolyse=0
                    E_h2[i]=E_h2[i-1]
                else:
                    E_h2[i]=E_h2[i-1]
                    P_fuelcell=0
                
            E_bat_new = E_bat[i-1]+(P_battery_charge*math.sqrt(eff_battery)-P_battery_discharge/math.sqrt(eff_battery))/60/1000
            if E_bat_new<=E_battery_max and E_bat_new>=0:
                E_bat[i]=E_bat_new
            else:
                E_bat[i]=E_bat[i-1]
                P_battery_charge=0
                P_battery_discharge=0
            
        P_elektrolyse_array[i] = P_elektrolyse
        P_battery_charge_array[i] = P_battery_charge
        P_fuelcell_array[i] = P_fuelcell
        P_battery_discharge_array[i] = P_battery_discharge
    return E_h2, E_bat, P_elektrolyse_array, P_fuelcell_array, P_battery_charge_array, P_battery_discharge_array
    



def batterie(P_dif, P_battery_charge_max, P_battery_discharge_max, eff_battery, E_battery_max):
    E_bat, P_battery_charge_array, P_battery_discharge_array=batterie_lastprofile(P_dif, P_battery_charge_max, P_battery_discharge_max, eff_battery, E_battery_max)
    return E_bat

def batterie_lastprofile(P_dif, P_battery_charge_max, P_battery_discharge_max, eff_battery, E_battery_max):
    
    size=np.size(P_dif)
    E_bat = np.zeros(size)
    if E_battery_max>2000:
        E_bat[0]=E_battery_max/2
    P_battery_charge_array = np.zeros(size)
    P_battery_discharge_array = np.zeros(size)
    
    for i,value in enumerate(P_dif):
        if value>0:
            P_battery_discharge=0
            if value<=P_battery_charge_max:
                P_battery_charge=value
            else:
                P_battery_charge=P_battery_charge_max
        else:
            P_battery_charge=0 
            if -value<=P_battery_discharge_max:
                P_battery_discharge=-value
            else:
                P_battery_discharge=P_battery_discharge_max
        
        if i>0: 
            E_bat_new = E_bat[i-1]+(P_battery_charge*math.sqrt(eff_battery)-P_battery_discharge/math.sqrt(eff_battery))/60/1000
            if E_bat_new<=E_battery_max and E_bat_new>=0:
                E_bat[i]=E_bat_new
            else:
                E_bat[i]=E_bat[i-1]
                P_battery_charge=0
                P_battery_discharge=0
                
        P_battery_charge_array[i] = P_battery_charge
        P_battery_discharge_array[i] = P_battery_discharge
    return E_bat, P_battery_charge_array, P_battery_discharge_array



def RFC_lastprofile(P_dif, P_elektrolyse_max, P_fuelcell_max, eff_elektrolyse, eff_fuelcell,  E_h2_max, E_h2_start):
    size=np.size(P_dif)
    E_h2 = np.zeros(size)
    E_h2[0]=E_h2_start
    P_elektrolyse_array = np.zeros(size)
    P_fuelcell_array = np.zeros(size)
    
    for i,value in enumerate(P_dif):
        if value>0:
            P_fuelcell=0
            if value<=P_elektrolyse_max:
                P_elektrolyse=value
            else:
                P_elektrolyse=P_elektrolyse_max
        else:
            P_elektrolyse=0 
            if -value<=P_fuelcell_max:
                P_fuelcell=-value
            else:
                P_fuelcell=P_fuelcell_max
        
        if i>0: 
            E_h2_new = E_h2[i-1]+(P_elektrolyse*eff_elektrolyse-P_fuelcell/eff_fuelcell)/60/1000
            if E_h2_new<=E_h2_max and E_h2_new>=0:
                E_h2[i]=E_h2_new
            else:
                E_h2[i]=E_h2[i-1]
                P_elektrolyse=0
                P_fuelcell=0
                
        P_elektrolyse_array[i] = P_elektrolyse
        P_fuelcell_array[i] = P_fuelcell
    return E_h2, P_elektrolyse_array, P_fuelcell_array

def RFC(P_ueber,eff_brennstoffzelle, eff_elektrolyse):
    upperlim=max(P_ueber)
    #Histogramm Überproduktion erstellen
    n, bins = np.histogram(P_ueber,np.arange(0,upperlim,20))
    ##Häufigkeit über Leistung aufintegrieren --> Energie
    integral=np.zeros(np.size(n))
    for i, value in enumerate(n):
        if i>0:
            integral[i]=integral[i-1]+average(bins[i],bins[i-1])*value/1000/60
    ##Bei Überschreiten der Max-Leistung wird mit Max-Leistung weitergearbeitet und nicht abgeschaltet        
    for i, value in enumerate(n):    
        integral[i]=integral[i]+np.sum(n[i:np.size(n)])*bins[i]/60/1000
    ##Ergebnis    
    P_elektrolyse_max_array=bins[1:np.size(bins)]
    E_elektrolyse=integral*eff_elektrolyse
    return P_elektrolyse_max_array, E_elektrolyse*eff_brennstoffzelle

def RFC_mitSektorkopplung(P_ueber,eff_brennstoffzelle, eff_elektrolyse, eff_brennstoffzelle_waerme, eff_elektrolyse_waerme):
    upperlim=max(P_ueber)
    ##Histogramm Überproduktion erstellen
    n, bins = np.histogram(P_ueber,np.arange(0,upperlim,20))
    ##Häufigkeit über Leistung aufintegrieren --> Energie
    integral=np.zeros(np.size(n))
    for i, value in enumerate(n):
        if i>0:
            integral[i]=integral[i-1]+average(bins[i],bins[i-1])*value/1000/60
    ##Bei Überschreiten der Max-Leistung wird mit Max-Leistung weitergearbeitet und nicht abgeschaltet        
    for i, value in enumerate(n):    
        integral[i]=integral[i]+np.sum(n[i:np.size(n)])*bins[i]/60/1000
    ##Ergebnis    
    P_elektrolyse_max_array=bins[1:np.size(bins)]
    E_h2=integral*eff_elektrolyse
    E_h2_elektro_netto = E_h2*eff_brennstoffzelle
    E_h2_waerme_netto = integral*eff_elektrolyse_waerme+E_h2*eff_brennstoffzelle_waerme
    #ax=vis.plot(P_elektrolyse_max_array/1000, E_h2_elektro_netto, 'Maximalleistung Elektrolyseur in kW','Überschussenergie in kWh', 'Einfluss Elektrolyseur Größe auf Nutzbarkeit der elektr. Überschussenergie')
    #vis.addtoplot(ax, P_elektrolyse_max_array/1000, E_h2_waerme_netto,'','best')
    #vis.addtoplot(ax,[0, upperlim/1000],[E_ueber,E_ueber],['E elektr. nach ein-aus-speicherung verfügbar', 'E waerme nach ein-ausspeicherung verfügbar','theoretisch ges. verfügbar '+"%4.0f" % E_ueber +'kWh'],'best')

    return P_elektrolyse_max_array, E_h2_elektro_netto, E_h2_waerme_netto

def P2G_lastprofile(P_ueber, P_elektrolyse_max, eff_elektrolyse):
    size=np.size(P_ueber)
    E_h2_array = np.zeros(size)
    P_elektrolyse_array = np.zeros(size)
    P_elektrolyse=0
    for i,value in enumerate(P_ueber):
        if value<=P_elektrolyse_max:
            P_elektrolyse=value
        else:
            P_elektrolyse=P_elektrolyse_max
                
        if i>0:
            E_h2_array[i] = E_h2_array[i-1]+P_elektrolyse*eff_elektrolyse/60/1000

        P_elektrolyse_array[i] = P_elektrolyse
        
    return E_h2_array, P_elektrolyse_array

def P2G(P_ueber,eff_brennstoffzelle, eff_elektrolyse):
    upperlim=max(P_ueber)
    #Histogramm Überproduktion erstellen
    n, bins = np.histogram(P_ueber,np.arange(0,upperlim,20))
    ##Häufigkeit über Leistung aufintegrieren --> Energie
    integral=np.zeros(np.size(n))
    for i, value in enumerate(n):
        if i>0:
            integral[i]=integral[i-1]+average(bins[i],bins[i-1])*value/1000/60
    ##Bei Überschreiten der Max-Leistung wird mit Max-Leistung weitergearbeitet und nicht abgeschaltet        
    for i, value in enumerate(n):    
        integral[i]=integral[i]+np.sum(n[i:np.size(n)])*bins[i]/60/1000
    ##Ergebnis    
    P_elektrolyse_max_array=bins[1:np.size(bins)]
    E_h2=integral*eff_elektrolyse
    return P_elektrolyse_max_array, E_h2

