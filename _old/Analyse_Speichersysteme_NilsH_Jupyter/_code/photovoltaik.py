import numpy as np

from numpy import pi
from scipy.ndimage.interpolation import shift


def rad(deg): #Umrechnung Grad in Radiant
    return deg*2*pi/360

def calculate_power(G_h, z, P_peak, PR, G_stc, v, inclination):

    from datetime import datetime
    #Array t_min mit Tageszeit in Minuten für spätere Berechnungen
    t_year=np.arange('2019-01-01', '2019-12-31 23', dtype='datetime64[m]')
    t_min = np.zeros(np.size(t_year))
    for nr, value in enumerate(t_year):
        t_new = t_year[nr].astype(datetime)
        t_min[nr] = t_new.hour*60+t_new.minute

    stdmedian=15 #Standardmedian abhängig von der Zeitzone
    laengengrad=9.199547
    breitengrad= 48.832287 
    omega=(t_min/60-12)*360/24-(stdmedian-laengengrad)

    G_inPlane = G_h*np.cos(rad(z-inclination))*np.cos(rad(omega-v))
    for i, value in enumerate(G_inPlane):
        if value<0:
            G_inPlane[i] = 0

    P_pv = G_inPlane*PR*P_peak/G_stc
    P_pv=np.concatenate((P_pv, np.zeros(60)), axis=0)
    return P_pv