import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.dates as mdates
import matplotlib.units as munits
import datetime
from matplotlib.ticker import FormatStrFormatter
import matplotlib.cm as cm
import matplotlib.colors as mcolors

#Datumformat erzeugen
converter = mdates.ConciseDateConverter()
munits.registry[np.datetime64] = converter
munits.registry[datetime.date] = converter
munits.registry[datetime.datetime] = converter

#Colormap erzeugen
colors=mcolors.TABLEAU_COLORS
ind=colors.keys()
c=[0] * len(ind)
for i,value in enumerate(ind):
    c[i]=colors[value]

#parameter für plots
fontsize=18
linewidth=2.5
opacity=0.7

def plot_timeline(*arg): #arg = timeline, data, ylabel, title, colourindex
    ax=0
    if len(arg)<2:
        print('Visualisierung Fehler - zu wenig Argumente\n - Minimum: plot_timeline(timeline,data)\n - Maximal: plot_timeline(timeline,data,ylabel,title,colourindex)')
    else:
        timeline=arg[0]
        data=arg[1]
        

        startelement=np.size(np.arange('2019-01-01',timeline[0], dtype='datetime64[m]'))
        fig, ax = plt.subplots(1, figsize=(17, 4))
        if len(arg)>4:
            colourindex=arg[4]
            ax.plot(timeline,data[startelement:startelement+np.size(timeline)], linewidth=linewidth,c=cm.tab10(colourindex))
        else:
            ax.plot(timeline,data[startelement:startelement+np.size(timeline)], linewidth=linewidth)
        if len(arg)>2:
            ax.set_ylabel(arg[2], fontsize=fontsize)
        if len(arg)>3:
            ax.set_title(arg[3], fontsize=fontsize)   
        ax.grid(True)
        plt.xticks(fontsize=fontsize)
        plt.yticks(fontsize=fontsize)
        plt.tight_layout()  
    return ax


def addtoplot_timeline(*arg): #arg=ax, timeline, data, legend, pos, colourindex
    if len(arg)<3:
        print('Visualisierung Fehler - zu wenig Argumente\n - Minimum: addtoplot_timeline(ax,timeline,data)\n - Maximal: addtoplot_timeline(ax,timeline,data,legend,pos,colourindex)')
    else:
        ax=arg[0]
        timeline=arg[1]
        data=arg[2]
        startelement=np.size(np.arange('2019-01-01',timeline[0], dtype='datetime64[m]'))
        if len(arg)>5:
            colourindex=arg[5]
            ax.plot(timeline,data[startelement:startelement+np.size(timeline)],linewidth=linewidth,c=cm.tab10(colourindex))
        else:
            ax.plot(timeline,data[startelement:startelement+np.size(timeline)],linewidth=linewidth)
        if len(arg)>3:
            legend=arg[3]
            ax.legend(legend,loc='best', fontsize=fontsize)
        if len(arg)>4:
            pos=arg[4]
            ax.legend(legend,loc=pos, fontsize=fontsize)
        plt.tight_layout()
    return

def plot(*arg):
    #arg = x, y, xlabel, ylabel, title, colourindex
    fig, ax = plt.subplots(1, figsize=(17, 5))
    if len(arg)>1:
        if len(arg)>5:
            ax.plot(arg[0],arg[1], linewidth=linewidth,c=cm.tab10(arg[5]))
        else:
            ax.plot(arg[0],arg[1], linewidth=linewidth)
    else:
        ax.plot(arg[0], linewidth=linewidth)
    if len(arg)>3:
        ax.set_ylabel(arg[3], fontsize=fontsize)
        ax.set_xlabel(arg[2], fontsize=fontsize)
    if len(arg)>4:
        ax.set_title(arg[4], fontsize=fontsize)   
    ax.grid(True)
    plt.xticks(fontsize=fontsize)
    plt.yticks(fontsize=fontsize)
    plt.tight_layout()
    return ax

def addtoplot(*arg):    #arg = ax, x, y, legend, pos, colourindex
    if len(arg)<2:
        print('Visualisierung Fehler - zu wenig Argumente\n - Minimum: addtoplot(ax,data)\n - Maximal: addtoplot(ax,x,y,legend,pos,colourindex)')
    else:
        ax=arg[0]
        if len(arg)<3:
            ax.plot(arg[1], linewidth=linewidth)
        else:
            if len(arg)>5:
                ax.plot(arg[1], arg[2], linewidth=linewidth,c=cm.tab10(arg[5]))
            else:
                ax.plot(arg[1], arg[2], linewidth=linewidth)
        if len(arg)>3:   
            ax.legend(arg[3],loc='best',fontsize=fontsize)
        if len(arg)>4:
            ax.legend(arg[3],loc=arg[4],fontsize=fontsize)
        plt.tight_layout()
    return

def save_to_tex(filename,ax): #Speichert letzten Plot als png file im hier angegebenen Pfad und erzeugt den passenden include Text für LaTex
    title=ax.get_title()
    try:
        path="""C:/Users/nilsh/Google Drive/Master_Reutlingen/03_Projektarbeiten/Masterthesis/Ausarbeitung/images/"""
        ax.set_title('')
        plt.savefig(path+filename+'.png')
        ax.set_title(title,fontsize=fontsize)
        imageinclude=r"""
\begin{figure}[H]
\includegraphics[width=\textwidth,center=\textwidth]{"""+filename+""".png}
\caption{"""+title+"""}
\label{fig:"""+filename+"""}
\end{figure}
        """
        with open(path+'image_includetext.tex') as f:
            if imageinclude in f.read():
                print('')
            else:
                f.close()
                f=open(path+'image_includetext.tex','a')
                f.write(imageinclude)
                f.close()
    except IOError:
        print("safe_to_tex Fehler: Speicherort nicht gefunden --> Pfad in visualisation.py anpassen")
    return
            
def histogram(bins, data, xlabel, ylabel, title):
    fig, ax = plt.subplots(1, figsize=(17, 4))
    n, bins, _ = plt.hist(data, bins)
    ax.set_xlabel(xlabel, fontsize=fontsize)
    ax.set_ylabel(ylabel, fontsize=fontsize)
    ax.set_title(title, fontsize=fontsize)
    ax.grid(True)
    plt.xticks(fontsize=fontsize)
    plt.yticks(fontsize=fontsize)
    plt.tight_layout()
    return bins, n, ax

def balkendiagramm_nebeneinander(xdata_,ydata, bins, xlabel, ylabel, title):
    plt.rc('xtick', labelsize=fontsize) 
    plt.rc('ytick', labelsize=fontsize) 
    fig, ax = plt.subplots(np.shape(ydata)[0], 2, figsize=(17, 6), gridspec_kw={'width_ratios': [4, 1]})
    if np.shape(ydata)[0]>1:
        ax=[0]*len(ydata)
        for i,value in enumerate(ydata):
            ax0=plt.subplot2grid((np.shape(ydata)[0], 4), (0, 0),rowspan = np.shape(ydata)[0], colspan = 3)
            ax[i] = plt.subplot2grid((np.shape(ydata)[0], 4),(i, 3), rowspan=1)
    else:
        ax0=ax[0]
        ax=[ax[1]]
    order=[0]*np.shape(ydata)[0]
    foreground=[0]*np.shape(ydata)[0]
    for i,value in enumerate(ydata):
        order[i]=value[0]   
    for i,value in enumerate(order):
        maxvalue = max(order)
        maxpos = order.index(maxvalue)
        order[maxpos]=min(order)-1
        foreground[maxpos]=i
    for i,value in enumerate(ydata):
        ax1=ax[i]
        xdata = [str(value) for value in xdata_]
        ax0.bar(xdata,value, edgecolor="black", linewidth=1,zorder=foreground[i],alpha=opacity)
        ax0.set_title(title, fontsize=fontsize)
        ax0.grid(axis='y')
        ax0.set_xlabel(xlabel, fontsize=fontsize)
        ax0.set_ylabel(ylabel, fontsize=fontsize)
        #plt.xticks(fontsize=fontsize)
        #plt.yticks(fontsize=fontsize)
        ax0.set_axisbelow(True)
        ax0.xaxis.set_major_formatter(FormatStrFormatter('%.0f'))
        if np.size(xdata_)>=70:
            ax0.xaxis.set_ticks(np.arange(0,75,5))
        #ax0.xaxis.set_ticks(np.arange(0,75,5))
        ax1.hist(value, bins,color=c[i], edgecolor="black", linewidth=1,zorder=foreground[i],alpha=opacity)
        ax1.set_ylabel('Häufigkeit', fontsize=fontsize)
        ax1.grid(axis='y')
        ax1.tick_params(axis='x', labelrotation= 45)
        ax1.set_axisbelow(True)
        stdabw=np.std(value)
        if stdabw>100 or stdabw<-100:
            str_stdabw=str(int(stdabw))
        else:
            str_stdabw=str(stdabw)[0:4]
        mean=np.mean(value)
        if mean>100 or mean<-100:
            str_mean=str(int(mean))
        else:
            str_mean=str(mean)[0:4]
        ax1.text(.95, .9, '\u03C3 = '+str_stdabw ,ha='right',va='top',transform=ax1.transAxes,fontsize=fontsize-4,bbox=dict(facecolor='white', alpha=0.5))
        ax1.text(.05, .9, '\u03bc = '+str_mean,ha='left',va='top',transform=ax1.transAxes,fontsize=fontsize-4,bbox=dict(facecolor='white', alpha=0.5))
    ax1.set_xlabel(ylabel, fontsize=fontsize)
    plt.tick_params(axis='both', which='major', labelsize=fontsize)
    plt.tight_layout()
    return ax0

def balkendiagramm(xdata_,ydata, lim, xlabel, ylabel, title):
    plt.rc('xtick', labelsize=fontsize) 
    plt.rc('ytick', labelsize=fontsize) 
    lowerlim=lim[0]
    upperlim=lim[1]
    bins=np.linspace(lowerlim,upperlim,int(80/np.shape(ydata)[0]))
    fig, ax = plt.subplots(2,np.shape(ydata)[0], figsize=(17, 10))
    if np.shape(ydata)[0]>1:
        ax=[0]*len(ydata)
        for i,value in enumerate(ydata):
            ax0=plt.subplot2grid((2,np.shape(ydata)[0]), (0, 0),colspan = np.shape(ydata)[0], rowspan = 1)
            ax[i] = plt.subplot2grid((2,np.shape(ydata)[0]),(1, i), colspan=1)
    else:
        #ax0=ax[0]
        ax0=plt.subplot2grid((2, 1), (0, 0), colspan = 1)
        #ax=[ax[1]]
        ax1 = plt.subplot2grid((2, 1),(1, 0), colspan = 1)
        ax=[ax1]
    order=[0]*np.shape(ydata)[0]
    foreground=[0]*np.shape(ydata)[0]
    for i,value in enumerate(ydata):
        order[i]=value[0]   
    for i,value in enumerate(order):
        maxvalue = max(order)
        maxpos = order.index(maxvalue)
        order[maxpos]=min(order)-1
        foreground[maxpos]=i
    ax[0].set_xlabel(ylabel, fontsize=fontsize)
    ax[0].set_ylabel('Häufigkeit', fontsize=fontsize)
    for i,value in enumerate(ydata):
        ax1=ax[i]
        xdata = [str(value) for value in xdata_]
        ax0.bar(xdata,value, edgecolor="black", linewidth=1,zorder=foreground[i],alpha=opacity)
        ax0.set_title(title, fontsize=fontsize)
        ax0.grid(axis='y')
        ax0.set_xlabel(xlabel, fontsize=fontsize)
        ax0.set_ylabel(ylabel, fontsize=fontsize)
        #plt.xticks(fontsize=fontsize)
        #plt.yticks(fontsize=fontsize)
        ax0.set_axisbelow(True)
        ax0.xaxis.set_major_formatter(FormatStrFormatter('%.0f'))
        if np.size(xdata_)>=70:
            ax0.xaxis.set_ticks(np.arange(0,75,5))
        #ax0.xaxis.set_ticks(np.arange(0,75,5))
        ax1.hist(value, bins,color=c[i], edgecolor="black", linewidth=1,zorder=foreground[i],alpha=opacity)
        ax1.grid(axis='y')
        ax1.tick_params(axis='x', labelrotation= 45)
        ax1.set_axisbelow(True)
        stdabw=np.std(value)
        if stdabw>100 or stdabw<-100:
            str_stdabw=str(int(stdabw))
        else:
            str_stdabw=str(stdabw)[0:4]
        mean=np.mean(value)
        if mean>100 or mean<-100:
            str_mean=str(int(mean))
        else:
            str_mean=str(mean)[0:4]
        ax1.text(.95, .9, '\u03C3 = '+str_stdabw ,ha='right',va='top',transform=ax1.transAxes,fontsize=fontsize-4,bbox=dict(facecolor='white', alpha=0.5))
        ax1.text(.05, .9, '\u03bc = '+str_mean,ha='left',va='top',transform=ax1.transAxes,fontsize=fontsize-4,bbox=dict(facecolor='white', alpha=0.5))
    plt.tick_params(axis='both', which='major', labelsize=fontsize)
    ax0.set_ylim(lowerlim,upperlim)
    ax0.grid(axis='y')
    plt.tight_layout()
    return ax0
            
def plot_korrelation(*arg):
    #arg = x, y, xlabel, ylabel, title
    global original_title
    original_title=''
    fig, ax = plt.subplots(1, figsize=(17, 5))
    if len(arg)>1:
        ax.plot(arg[0],arg[1],'yo')
    if len(arg)>3:
        ax.set_ylabel(arg[3], fontsize=fontsize)
        ax.set_xlabel(arg[2], fontsize=fontsize)
    if len(arg)>4:
        original_title=arg[4]
        ax.set_title(arg[4], fontsize=fontsize)   
    ax.grid(True)
    plt.xticks(fontsize=fontsize)
    plt.yticks(fontsize=fontsize)
    plt.tight_layout()
    return ax
            
