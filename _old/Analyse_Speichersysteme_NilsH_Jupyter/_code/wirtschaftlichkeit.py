import numpy as np
import visualisation as vis
import matplotlib.pyplot as plt


def RFC(E_jahr_RFC,eff_elektrolyse,eff_brennstoffzelle,P_max_RFC_array,K_system_H2,c_netz,c_einspeisung):
    ##RFC ohne Sektorkopplung
    ##Kostenersparnis bei vollständiger Rückumwandlung mit Brennstoffzelle
    first=0
    for value in K_system_H2:
        Kosteneinsparung=E_jahr_RFC*c_netz-P_max_RFC_array*value-c_einspeisung*E_jahr_RFC/eff_elektrolyse/eff_brennstoffzelle
        if first==0:
            ax=vis.plot(P_max_RFC_array/1000, Kosteneinsparung,
            'Maximalleistung in kW', 'Kosteneinsparung in €/Jahr', \
            'Wirtschaftlichkeit Wasserstoffrückverstromung')
            legend = ['Speichersystemkosten'+ "%4.0f" % (value*1000) +'€/kW/Jahr']
            first=1
            Kosteneinsparung_array=[Kosteneinsparung]
        else:
            legend.append("%4.0f" % (value*1000) +'€/kW/Jahr')
            vis.addtoplot(ax,P_max_RFC_array/1000, Kosteneinsparung, legend,'best')
            Kosteneinsparung_array.append(Kosteneinsparung)     
    plt.ylim(-100,1000);
    return ax, Kosteneinsparung_array


def RFC_mit_sektorkopplung(E_jahr_RFCsektorkopplung,E_jahr_RFCsektorkopplung_waerme,eff_elektrolyse,eff_brennstoffzelle,P_max_RFC_array,K_system_H2,c_netz,c_einspeisung):
    ##Wasseerstoffspeicher mit Sektorkopplung
    #ax=vis.plot(P_max_RFC_array_sektor, E_jahr_RFCsektorkopplung,'Maxleistung Elektrolyseur in W','zusätzlich verfügbare \n Energie in kWh','RFC mit Sektorkopplung')
    #vis.addtoplot(ax,P_max_RFC_array_sektor, E_jahr_RFCsektorkopplung_waerme,['Elektrische Energie','Wärme Energie'],'best')

    ##Kostenersparnis bei Wärmeenergievergütung 6ct/kWh
    
    first=0
    for value in K_system_H2:
        Kosteneinsparung=E_jahr_RFCsektorkopplung*c_netz-P_max_RFC_array*value+E_jahr_RFCsektorkopplung_waerme*0.06-c_einspeisung*E_jahr_RFCsektorkopplung/eff_elektrolyse/eff_brennstoffzelle
        if first==0:
            ax=vis.plot(P_max_RFC_array/1000, Kosteneinsparung,
            'Maximalleistung in kW', 'Kosteneinsparung in €/Jahr', \
            'Wirtschaftlichkeit Wasserstoffrückverstromung mit Sektorkopplung')
            legend = ['Speichersystemkosten'+ "%4.0f" % (value*1000) +'€/kW/Jahr']
            first=1
            Kosteneinsparung_array=[Kosteneinsparung]
        else:
            legend.append("%4.0f" % (value*1000) +'€/kW/Jahr')
            vis.addtoplot(ax,P_max_RFC_array/1000, Kosteneinsparung, legend,'best')
            Kosteneinsparung_array.append(Kosteneinsparung)
    plt.ylim(-100,1000);
    return ax, Kosteneinsparung_array

def batterie(E_jahr_bat,E_bat_max_array,eff_batterie,c_netz,c_einspeisung,systemkosten_bat):
    Kosteneinsparung=E_jahr_bat*c_netz-E_bat_max_array*systemkosten_bat-c_einspeisung*E_jahr_bat/eff_batterie
    ax=vis.plot(E_bat_max_array,Kosteneinsparung,'Speichergröße in kWh','Kosteneinsparung in €/Jahr','Wirtschaftlichkeit Batteriespeicher')      
    plt.ylim(-100,1000);
    return ax,Kosteneinsparung

def P2G(P_max_P2G_array,E_jahr_P2G,eff_elektrolyse,K_system_H2,Wasserstoffpreis,c_einspeisung):
    first=0
    for value in K_system_H2:
        Kosteneinsparung=E_jahr_P2G*Wasserstoffpreis-P_max_P2G_array*value-c_einspeisung*E_jahr_P2G/eff_elektrolyse
        if first==0:
            ax=vis.plot(P_max_P2G_array/1000, Kosteneinsparung,
            'Maximalleistung in kW', 'Kosteneinsparung in €/Jahr', \
            'Wirtschaftlichkeit Wasserstoffverkauf')
            legend = ['Systemkosten'+ "%4.0f" % (value*1000) +'€/kW/Jahr']
            first=1
            Kosteneinsparung_array=[Kosteneinsparung]
        else:
            legend.append("%4.0f" % (value*1000) +'€/kW/Jahr')
            vis.addtoplot(ax,P_max_P2G_array/1000, Kosteneinsparung, legend,'best')
            Kosteneinsparung_array.append(Kosteneinsparung)
    plt.ylim(-100,1000);
    return ax,Kosteneinsparung

def vergleich(E_verbraucher,E_def,E_jahr_RFC,Kosteneinsparung_ohneSektorkopplung,systemkosten_ohneSektorkopplung,E_jahr_RFCsektorkopplung,Kosteneinsparung_mitSektorkopplung,systemkosten_mitSektorkopplung,E_jahr_bat,Kosteneinsparung_batterie,systemkosten_bat,lowerlim,upperlim,x_upperlim):
    autarkiegrad = (E_verbraucher-E_def+E_jahr_RFC)/E_verbraucher*100
    first=0
    for i,value in enumerate(Kosteneinsparung_ohneSektorkopplung):
        if first==0:
            legend = ['RFC'+"%4.0f" % systemkosten_ohneSektorkopplung[i] +'€/kW/Jahr']
            ax=vis.plot(autarkiegrad, value,'Autarkiegrad in %', 'Kosteneinsparung in €/Jahr','Wirtschaftlichkeitsvergleich Speichersysteme')
            first=1
        else:
            legend.append('RFC'+"%4.0f" % systemkosten_ohneSektorkopplung[i] +'€/kW/Jahr')
            vis.addtoplot(ax,autarkiegrad, value)
    
    for i,value in enumerate(Kosteneinsparung_mitSektorkopplung):
        legend.append('RFC mit Sektorkopplung'+"%4.0f" % systemkosten_mitSektorkopplung[i] +'€/kW/Jahr')
        autarkiegrad = (E_verbraucher-E_def+E_jahr_RFCsektorkopplung)/E_verbraucher*100
        vis.addtoplot(ax,autarkiegrad, value)
    
    #Batteriespeicher
    autarkiegrad = (E_verbraucher-E_def+E_jahr_bat)/E_verbraucher*100
    legend.append('Batteriespeicher '+"%4.0f" % systemkosten_bat +'€/kWh/Jahr')
    vis.addtoplot(ax,autarkiegrad, Kosteneinsparung_batterie)
    
    plt.legend(legend,fontsize=12)
    plt.ylim(lowerlim,upperlim)
    plt.xlim(0,x_upperlim);
    
    return ax


