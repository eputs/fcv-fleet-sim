import os
import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import savgol_filter
import sys
import datetime
import visualisation as vis
import input_read as read
import timeline
import photovoltaik as pv
import speicher
from own_utility_functions import *
import wirtschaftlichkeit
import umwelteinfluss

h0_1000 = read.H0()
year_dayres = timeline.day_period('2019-01-01','2019-12-31')

def simulation(LastprofilNr,P_verbraucher,P_pv,E_bat_max_array,P_bat_charge_max,\
               P_bat_discharge_max,eff_bat,eff_brennstoffzelle, eff_elektrolyse,K_system_wasserstoff,K_system_batterie, c_netz, \
               c_einspeisung,wasserstoffverguetung,Strommix_CO2,H2System_CO2,Dampfreformierung_CO2, Batterie_CO2,detailplot_optionen):
    tic()
    #######################################################################
    plot_detail=0
    detailpath=0
    if detailplot_optionen[0] == 'Detailplot':
        plot_detail=1
        if 1<len(detailplot_optionen):
            timeline=detailplot_optionen[1]
        else:
            timeline=year
        if 2<len(detailplot_optionen):
            detailpath=detailplot_optionen[2]
        
        
    sim_output_dict={}
    Nummer=str(int(LastprofilNr))
    print('Lastprofilnr.',Nummer);
    ##Beginn Simulationsfortschrittanzeige
    print('<Simulationsfortschritt>')
    ########################################################################
        
    ##Lastprofile und Energiebilanzen berechnen#############################
    P_dif = P_pv-P_verbraucher
    P_def = negval_cutoff(-P_dif)
    P_ueber = negval_cutoff(P_dif)
    E_ueber = jahresenergie_kWh(P_ueber)
    E_verbraucher = jahresenergie_kWh(P_verbraucher)
    E_netz = jahresenergie_kWh(P_def)
    E_def=E_netz
    E_eigen = E_verbraucher-E_netz
    #H0 Profil als Vergleich
    P_verbraucher_h0=h0_1000*E_verbraucher/1000
    P_dif_h0 = P_pv-P_verbraucher_h0
    P_def_h0 = negval_cutoff(-P_dif_h0)
    P_ueber_h0 = negval_cutoff(P_dif_h0)
    E_ueber_h0 = jahresenergie_kWh(P_ueber_h0)
    E_netz_h0 = jahresenergie_kWh(P_def_h0)
    E_def_h0=E_netz_h0
    E_eigen_h0 = E_verbraucher-E_netz_h0
    #in output dict schreiben
    sim_output_dict['E_netz_jahr'] = E_netz
    sim_output_dict['E_eigen_ohne_jahr'] = E_eigen
    sim_output_dict['E_eigen_ohne_H0_jahr'] = E_eigen_h0
    #######################################################################
    
    
    ##Speicherauslegung####################################################
    #Batterie
    first=0
    E_jahr_bat=np.zeros(np.size(E_bat_max_array))
    for i,value in enumerate(E_bat_max_array):
        E_bat,P_bat_charge,P_bat_discharge = speicher.batterie_lastprofile(P_dif, P_bat_charge_max, \
            P_bat_discharge_max, eff_bat, value)
        E_jahr_bat[i] = jahresenergie_kWh(P_bat_discharge)
        print('.',end="")#Simulationsfortschritt
    #Wasserstoffrückverstromung/RFC
    P_RFC_max_array, E_jahr_RFC=speicher.RFC(P_ueber, eff_brennstoffzelle, eff_elektrolyse)
    E_jahr_RFC=upperlim_cutoff(E_jahr_RFC,E_def) #Falls 100% Autarkie erreicht wird alles darüber abschneiden
    #Wasserstoffverkauf/P2G
    P_P2G_max_array, E_jahr_P2G=speicher.P2G(P_ueber, eff_brennstoffzelle, eff_elektrolyse)
    print('.',end="")#Simulationsfortschritt
    #########################################################################
    
    
    ###Kennzahlen
    ##Kosteneinsparung#######################################################
    #Batterie
    ax1, K_einspar_bat_array=wirtschaftlichkeit.batterie(E_jahr_bat,E_bat_max_array,eff_bat,\
        c_netz,c_einspeisung,K_system_batterie);
    if plot_detail==0:
        plt.close()
    else:
        if detailpath!=0:
            plt.savefig(detailpath+'Kosteneinsparung_Batterie.png')
    K_einspar_bat=max(K_einspar_bat_array)
    #Wasserstoffrückverstromung/RFC
    ax2, K_einspar_RFC_array=wirtschaftlichkeit.RFC(E_jahr_RFC,eff_elektrolyse,\
        eff_brennstoffzelle,P_RFC_max_array,[K_system_wasserstoff],c_netz,c_einspeisung);
    K_einspar_RFC_array=K_einspar_RFC_array[0]
    if plot_detail==0:
        plt.close()
    else:
        if detailpath!=0:
            plt.savefig(detailpath+'Kosteneinsparung_Wasserstoffrueckverstromung.png')
    K_einspar_RFC=max(K_einspar_RFC_array)
    #Wasserstoffverkauf/P2G
    ax3, K_einspar_P2G_array=wirtschaftlichkeit.P2G(P_P2G_max_array,E_jahr_P2G,eff_elektrolyse,\
        [K_system_wasserstoff],wasserstoffverguetung,c_einspeisung);
    if plot_detail==0:
        plt.close()
    else:
        if detailpath!=0:
            plt.savefig(detailpath+'Kosteneinsparung_Wasserstoffverkauf.png')
    K_einspar_P2G=max(K_einspar_P2G_array)
    #in output dict schreiben
    sim_output_dict['K_einspar_RFC'] = K_einspar_RFC
    sim_output_dict['K_einspar_P2G'] = K_einspar_P2G
    sim_output_dict['K_einspar_bat'] = K_einspar_bat
    print('.',end="")#Simulationsfortschritt
    
    ymax=max([K_einspar_RFC,K_einspar_P2G,K_einspar_bat])
    ymin=min([K_einspar_RFC,K_einspar_P2G,K_einspar_bat])
    if ymin>0:
        ymin=0
    ax1.set_ylim(ymin-100,ymax+100)
    ax2.set_ylim(ymin-100,ymax+100)
    ax3.set_ylim(ymin-100,ymax+100)
    #########################################################################
    
    
    ##Optimalauslegung#######################################################
    #Batterie
    i_kosten_opt_bat=find_value(K_einspar_bat_array,K_einspar_bat)
    E_max_bat_kosten_opt=E_bat_max_array[i_kosten_opt_bat]
    i_CO2_opt_bat=findr_value(K_einspar_bat_array,0)
    E_max_bat_CO2_opt=E_bat_max_array[i_CO2_opt_bat]
    if K_einspar_bat<0 or E_max_bat_kosten_opt<0.5 or E_max_bat_CO2_opt<0.5:
        K_einspar_bat=0
        E_max_bat_kosten_opt=0
        E_max_bat_CO2_opt=0
        i_CO2_opt_bat=0
        i_kosten_opt_bat=0
    #RFC
    i_kosten_opt_RFC=find_value(K_einspar_RFC_array,K_einspar_RFC)
    P_max_RFC_kosten_opt=P_RFC_max_array[i_kosten_opt_RFC]
    i_CO2_opt_RFC=findr_value(K_einspar_RFC_array,0)
    P_max_RFC_CO2_opt=P_RFC_max_array[i_CO2_opt_RFC]
    if K_einspar_RFC<0 :
        K_einspar_RFC=0
        P_max_RFC_kosten_opt=0
        P_max_RFC_CO2_opt=0
        i_CO2_opt_RFC=0
        i_kosten_opt_RFC=0
    #P2G
    i_kosten_opt_P2G=find_value(K_einspar_P2G_array ,K_einspar_P2G)
    P_max_P2G_kosten_opt=P_P2G_max_array[i_kosten_opt_P2G]
    i_CO2_opt_P2G=findr_value(K_einspar_P2G_array,0)
    P_max_P2G_CO2_opt=P_P2G_max_array[i_CO2_opt_P2G]
    if K_einspar_P2G<0:
        K_einspar_P2G=0
        P_max_P2G_kosten_opt=0
        P_max_P2G_CO2_opt=0
        i_kosten_opt_P2G=0
        i_CO2_opt_P2G=0
    #in output dict schreiben
    sim_output_dict['P_max_RFC_kosten_opt'] = P_max_RFC_kosten_opt
    sim_output_dict['P_max_P2G_kosten_opt'] = P_max_P2G_kosten_opt
    sim_output_dict['E_max_bat_kosten_opt'] = E_max_bat_kosten_opt
    sim_output_dict['P_max_RFC_CO2_opt'] = P_max_RFC_CO2_opt
    sim_output_dict['P_max_P2G_CO2_opt'] = P_max_P2G_CO2_opt
    sim_output_dict['E_max_bat_CO2_opt'] = E_max_bat_CO2_opt
    print('.',end="")#Simulationsfortschritt
    ########################################################################
    
    
    ##Autarkiegrad##########################################################
    #Batterie
    autarkie_bat_kosten_opt=int((E_eigen+E_jahr_bat[i_kosten_opt_bat])/E_verbraucher*100)
    autarkie_bat_CO2_opt=int((E_eigen+E_jahr_bat[i_CO2_opt_bat])/E_verbraucher*100)
    #RFC
    autarkie_RFC_kosten_opt=int((E_eigen+E_jahr_RFC[i_kosten_opt_RFC])/E_verbraucher*100)
    autarkie_RFC_CO2_opt=int((E_eigen+E_jahr_RFC[i_CO2_opt_RFC])/E_verbraucher*100)
    #P2G
    autarkie_P2G_kosten_opt=int((E_eigen+E_jahr_P2G[i_kosten_opt_P2G]/eff_elektrolyse)/(E_verbraucher+E_jahr_P2G[i_kosten_opt_P2G]/eff_elektrolyse)*100)
    autarkie_P2G_CO2_opt=int((E_eigen+E_jahr_P2G[i_CO2_opt_P2G]/eff_elektrolyse)/(E_verbraucher+E_jahr_P2G[i_CO2_opt_P2G]/eff_elektrolyse)*100)
    #in output dict schreiben
    sim_output_dict['autarkie_RFC_kosten_opt'] = autarkie_RFC_kosten_opt
    sim_output_dict['autarkie_P2G_kosten_opt'] = autarkie_P2G_kosten_opt
    sim_output_dict['autarkie_bat_kosten_opt'] = autarkie_bat_kosten_opt
    sim_output_dict['autarkie_RFC_CO2_opt'] = autarkie_RFC_CO2_opt
    sim_output_dict['autarkie_P2G_CO2_opt'] = autarkie_P2G_CO2_opt
    sim_output_dict['autarkie_bat_CO2_opt'] = autarkie_bat_CO2_opt
    print('.',end="")#Simulationsfortschritt
    ########################################################################
    
    
    ##CO2-Emmisionen########################################################
    #ohne Speicher
    CO2_20Jahre_ohne=E_netz*Strommix_CO2*20
    CO2prokWh_ohne=CO2_20Jahre_ohne/20/E_verbraucher
    #Batterie
    CO2_20jahre_bat=(E_def-E_jahr_bat[i_CO2_opt_bat])*Strommix_CO2*20+Batterie_CO2*E_max_bat_CO2_opt
    CO2prokWh_bat=CO2_20jahre_bat/20/E_verbraucher
    #RFC
    CO2_20jahre_RFC=(E_def-E_jahr_RFC[i_CO2_opt_RFC])*Strommix_CO2*20+H2System_CO2*2*P_max_RFC_CO2_opt/1000
    CO2prokWh_RFC=CO2_20jahre_RFC/20/E_verbraucher
    #P2G
    CO2_20jahre_P2G=(20*E_netz*Strommix_CO2+H2System_CO2*P_max_P2G_CO2_opt/1000\
         -Dampfreformierung_CO2*E_jahr_P2G[i_CO2_opt_P2G]*20)
    CO2prokWh_P2G=CO2_20jahre_P2G/20/E_verbraucher
    #in output dict schreiben
    sim_output_dict['CO2prokWh_RFC'] = CO2prokWh_RFC
    sim_output_dict['CO2prokWh_P2G'] = CO2prokWh_P2G
    sim_output_dict['CO2prokWh_bat'] = CO2prokWh_bat
    sim_output_dict['CO2prokWh_ohne'] = CO2prokWh_ohne
    print('.',end="")#Simulationsfortschritt
    ########################################################################
    
    
    ##Simulationsfortschrittanzeige Beenden
    print('\n')
    toc()
    print('\n')
    
    
    ##Informationen zum Simulationsergebnis ausgeben
    if plot_detail>0:
        #tägliche Bilanz bzw Einspeisung und Netzbezug ohne Speicher
        #Glättungsfunktion Fenstergröße
        windowsize_smooth=21 #muss ungerade sein
        taegl_defizit = initialize_year_array('D',0)
        taegl_ueberschuss = initialize_year_array('D',0)
        for i, value in enumerate(P_dif):
            if i%(60*24)== 0:
                taegl_ueberschuss[int(i/60/24)] = np.sum(P_ueber[i-60*24:i])/60/1000
                taegl_defizit[int(i/60/24)] = np.sum(P_def[i-60*24:i])/60/1000
        taegl_defizit[0]=15
        taegl_ueberschuss_smooth = savgol_filter(taegl_ueberschuss, windowsize_smooth, 3)
        ax=vis.plot_timeline(year_dayres, taegl_ueberschuss_smooth,'Energie pro Tag in kWh','Jahresverlauf Einspeisung/Netzbezug ohne Speicher')
        legend=['Einspeisung']
        taegl_defizit_smooth = savgol_filter(taegl_defizit, windowsize_smooth, 3)
        legend.append('Netzbezug')
        vis.addtoplot_timeline(ax, year_dayres, taegl_defizit_smooth, legend,'upper right')
        ymax_jahresverlauf=max(taegl_ueberschuss)+10
        ax.set_ylim(0,ymax_jahresverlauf)
        
        #Batterie Optimalauslegung wirtschaftlichkeit############################################
        E_bat, P_battery_charge_array, P_battery_discharge_array=speicher.batterie_lastprofile(P_dif, P_bat_charge_max, P_bat_discharge_max, eff_bat, E_max_bat_kosten_opt)
        title='Speicherfüllstand Batterie Kostenoptimum für Lastprofil '+Nummer
        ax=vis.plot_timeline(timeline, E_bat,'Speicherfüllstand in kWh',title)
        title='Speicherleistung Batterie Kostenoptimum für Lastprofil '+Nummer
        ax=vis.plot_timeline(timeline, P_battery_charge_array-P_battery_discharge_array,'Speicherleistung in W',title)
        #tägliche Bilanz bzw Einspeisung und Netzbezug (wirtschaftlichste Auslegung)
        P_ueber_bat=P_ueber-P_battery_charge_array
        P_def_bat=P_def-P_battery_discharge_array
        #Glättungsfunktion Fenstergröße
        windowsize_smooth=21 #muss ungerade sein
        taegl_defizit = initialize_year_array('D',0)
        taegl_ueberschuss = initialize_year_array('D',0)
        for i, value in enumerate(P_dif):
            if i%(60*24)== 0:
                taegl_ueberschuss[int(i/60/24)] = np.sum(P_ueber_bat[i-60*24:i])/60/1000
                taegl_defizit[int(i/60/24)] = np.sum(P_def_bat[i-60*24:i])/60/1000
        taegl_defizit[0]=15
        taegl_ueberschuss_smooth = savgol_filter(taegl_ueberschuss, windowsize_smooth, 3)
        title='Jahresverlauf Einspeisung/Netzbezug mit Batterie Kostenoptimum '+str(int(E_max_bat_kosten_opt))+'kWh für Lastprofil '+ Nummer
        ax=vis.plot_timeline(year_dayres, taegl_ueberschuss_smooth,'Energie pro Tag in kWh',title)
        legend=['Einspeisung']
        taegl_defizit_smooth = savgol_filter(taegl_defizit, windowsize_smooth, 3)
        legend.append('Netzbezug')
        vis.addtoplot_timeline(ax, year_dayres, taegl_defizit_smooth, legend,'upper right')
        ax.set_ylim(0,ymax_jahresverlauf)
        
        
        #Batterie Optimalauslegung Ökologie##########################################################
        E_bat, P_battery_charge_array, P_battery_discharge_array=speicher.batterie_lastprofile(P_dif, P_bat_charge_max, P_bat_discharge_max, eff_bat, E_max_bat_CO2_opt)
        #tägliche Bilanz bzw Einspeisung und Netzbezug (wirtschaftlichste Auslegung)
        P_ueber_bat=P_ueber-P_battery_charge_array
        P_def_bat=P_def-P_battery_discharge_array
        #Glättungsfunktion Fenstergröße
        windowsize_smooth=21 #muss ungerade sein
        taegl_defizit = initialize_year_array('D',0)
        taegl_ueberschuss = initialize_year_array('D',0)
        for i, value in enumerate(P_dif):
            if i%(60*24)== 0:
                taegl_ueberschuss[int(i/60/24)] = np.sum(P_ueber_bat[i-60*24:i])/60/1000
                taegl_defizit[int(i/60/24)] = np.sum(P_def_bat[i-60*24:i])/60/1000
        taegl_defizit[0]=15
        taegl_ueberschuss_smooth = savgol_filter(taegl_ueberschuss, windowsize_smooth, 3)
        title='Jahresverlauf Einspeisung/Netzbezug mit Batterie CO2-Optimum'+str(int(E_max_bat_CO2_opt))+'kWh für Lastprofil '+ Nummer
        ax=vis.plot_timeline(year_dayres, taegl_ueberschuss_smooth,'Energie pro Tag in kWh',title)
        legend=['Einspeisung']
        taegl_defizit_smooth = savgol_filter(taegl_defizit, windowsize_smooth, 3)
        legend.append('Netzbezug')
        vis.addtoplot_timeline(ax, year_dayres, taegl_defizit_smooth, legend,'upper right')
        ax.set_ylim(0,ymax_jahresverlauf)
        
        
        plt.show()
    #end if    
        
        
        
        
        
    print('E_netz_jahr = ',E_netz)
    print('E_eigen_ohne_jahr = ',E_eigen)
    print('E_eigen_ohne_H0_jahr = ',E_eigen_h0)
    print('K_einspar_RFC = ',K_einspar_RFC)
    print('K_einspar_P2G = ',K_einspar_P2G)
    print('K_einspar_bat = ',K_einspar_bat)
    print('P_max_RFC_kosten_opt = ',P_max_RFC_kosten_opt)
    print('P_max_P2G_kosten_opt = ',P_max_P2G_kosten_opt)
    print('E_max_bat_kosten_opt = ',E_max_bat_kosten_opt)
    print('P_max_RFC_CO2_opt = ',P_max_RFC_CO2_opt)
    print('P_max_P2G_CO2_opt = ',P_max_P2G_CO2_opt)
    print('E_max_bat_CO2_opt = ',E_max_bat_CO2_opt)
    print('autarkie_RFC_kosten_opt = ',autarkie_RFC_kosten_opt)
    print('autarkie_P2G_kosten_opt = ',autarkie_P2G_kosten_opt)
    print('autarkie_bat_kosten_opt = ',autarkie_bat_kosten_opt)
    print('autarkie_RFC_CO2_opt = ',autarkie_RFC_CO2_opt)
    print('autarkie_P2G_CO2_opt = ',autarkie_P2G_CO2_opt)
    print('autarkie_bat_CO2_opt = ',autarkie_bat_CO2_opt)
    print('CO2prokWh_RFC = ',CO2prokWh_RFC)
    print('CO2prokWh_P2G = ',CO2prokWh_P2G)
    print('CO2prokWh_bat = ',CO2prokWh_bat)
    print('CO2prokWh_ohne = ',CO2prokWh_ohne,'\n\n')
    return sim_output_dict








