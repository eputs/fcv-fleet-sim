import numpy as np
import os.path
from os import path
import pathlib

def find_directory(directory):
    current_directory=str(pathlib.Path().absolute())
    input_directory=current_directory+directory
    i=0
    while not path.exists(input_directory):
        #print(input_directory)
        if i<10:
            i=i+1
        else:
            input_directory='Ordner wurde nicht gefunden'
            print(input_directory)
            break
        current_directory_new = current_directory[0:current_directory.rfind(os.sep)]
        input_directory=current_directory_new+'/'+directory
        current_directory = current_directory_new
        input_directory=input_directory+'/'
    return input_directory


def upperlim_cutoff(data, upperlim):
    result=np.zeros(len(data))
    for i,value in enumerate(data):
        if value>=upperlim:
            result[i]=upperlim
        else:
            result[i]=value
    return result

def lowerlim_cutoff(data, lowerlim):
    result=np.zeros(len(data))
    for i,value in enumerate(data):
        if value<=lowerlim:
            result[i]=lowerlim
        else:
            result[i]=value
    return result



def find_value(data, x):
    element=0
    for i,value in enumerate(data):
        element=i
        if value>=x:
            break
    return element

def findr_value(data, x):
    i=len(data)-1
    element=i
    value_old=data[-1]
    for value in reversed(data):
        if value>=value_old and value>=x:
            break
        #if value<value_old and value<x:
        #    break
        else:
            element=i
        i=i-1
        value_old=value
    return element

def negval_cutoff(data):
    result=np.copy(data)
    for i, value in enumerate(data):
        if value<0:
            result[i] = 0
    return result

def jahresenergie_kWh(P_minutes):
    return np.sum(P_minutes)/60/1000


def resulution_mintoday(data):
    result = np.zeros(int(np.size(data)/60/24))
    for i,value in enumerate(data):
        if i%(60*24)==0:
            result[int(i/60/24)]=value
    result[364]=result[363]
    return result

def resulution_daytomin(data):
    result = np.zeros(int(np.size(data)*60*24))
    for i,value in enumerate(data):
        result[i*60*24:i*60*24+60*24]=value
    return result

def initialize_year_array(res,columns):
    array = 0
    if columns:
        if res == 'm':
            array = np.zeros((365*24*60,columns))
        if res == 'D':
            array = np.zeros((365,columns))
    else:
        if res == 'm':
            array = np.zeros(365*24*60)
        if res == 'D':
            array = np.zeros(365)
    return array

import time

def TicTocGenerator():
    # Generator that returns time differences
    ti = 0           # initial time
    tf = time.time() # final time
    while True:
        ti = tf
        tf = time.time()
        yield tf-ti # returns the time difference

TicToc = TicTocGenerator() # create an instance of the TicTocGen generator

# This will be the main function through which we define both tic() and toc()
def toc(tempBool=True):
    # Prints the time difference yielded by generator instance TicToc
    tempTimeInterval = next(TicToc)
    if tempBool:
        print( "Elapsed time: %f seconds." %tempTimeInterval )

def tic():
    # Records a time in TicToc, marks the beginning of a time interval
    toc(False)
    
def average(a,b):
    return (a+b)/2