import os
import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import savgol_filter
import sys
import datetime
import visualisation as vis
import input_read as read
import timeline
import photovoltaik as pv
import speicher
from own_utility_functions import *
import wirtschaftlichkeit
import umwelteinfluss
currentDirectory = os.getcwd()

##Simulationsmodus Default initialisieren --> keine Simulation außer es wird überschrieben
mode0=0 #alle Lastprofile über Kennwerte vergleichen
mode1=0 #aufskalieren zu zentralem System für mehrere Haushalte
mode2=0 #verschiedene PV-Leistungen sonst wie mode 2
mode3=0 #Elektrolyseur Sommerbetrieb + Batterie

file_extension='best_case'

directory_data=find_directory('_data')
directory_output=find_directory('_output')

Lastprofile = read.HTW_profiles()
h0_1000 = read.H0()
G_h, z = read.solar_data()

#Zeitausschnitte für Visualisierung
days = timeline.minute_period('2019-05-22','2019-05-25')
year = timeline.minute_period('2019-01-01','2019-12-31')
year_dayres = timeline.day_period('2019-01-01','2019-12-31')


#Leistungen in Watt, Energie in kWh


#----------------------Photovoltaik Anlage----------------------------------------------------------------
P_peak=10000
PR=0.8
G_stc=1000
v=0          #Kollektorenausrichtung Abweichung von Südausrichtung in Grad
inclination = 35 #Kollektorwinkel zur horizontalen in Grad




#-------------Wasserstoffsystem----------------------------------------------------------------
elektrolyseurleistung_rep=2000

eff_elektrolyse = 0.85
eff_brennstoffzelle = 0.70

E_h2_max = 10000
E_h2_start = 0 #Speicherfüllstand zu Beginn des Jahrs

#Systemkosten in €/W
K_system_wasserstoff=150/1000

wasserstoffverguetung=0.28 #€/kWh

#Sektorkopplung
eff_gesamt=0.9 #aus http://enefield.eu/category/field-trials/technology/
eff_brennstoffzelle_waerme=eff_gesamt-eff_brennstoffzelle
eff_elektrolyse_waerme=eff_gesamt-eff_elektrolyse

#Kosten in Euro/kWh
c_netz = 0.3
c_einspeisung = 0
kosten_waermeenergie=0.06


#-----------------Batteriespeicher-----------------------------------------------------------------------
#Batteriekosten pro Jahr bei linearer Abschreibung über 20 Jahre mit Anschaffungskosten 1200€/kWh 
#(Quelle:https://www.energie-experten.org/erneuerbare-energien/photovoltaik/stromspeicher/preise)
batteriekapazitaet_rep=5
K_system_batterie=30

P_bat_charge_max = 3000
P_bat_discharge_max = 3000
eff_bat = 0.96
bat_res=20
E_bat_max_array = np.linspace(4,30,bat_res)


#--------------Umwelteinfluss---------------------------------------------
#CO2-Ausstoß Daten
Strommix_CO2 = 0.4 # kg/kWh
Batterie_CO2 = 150 # kg/kWh nutzbare Speicherkapazität
H2System_CO2 = 30 # kg/kW Stackleistung
Dampfreformierung_CO2 = 0.32 # kg/kWh Wasserstoff