#!/usr/bin/env python

from math import sqrt
import numpy as np
import datetime


def reserveProfile(args):

    minutes = np.arange(
        datetime.datetime(2020, 1, 1),
        datetime.datetime(2020, 12, 31),
        datetime.timedelta(minutes=1),
    ).astype(datetime.datetime)
    reserve = np.zeros(len(minutes))
    for x in args:
        for i, val in enumerate(minutes):
            if x["Monate"][0] <= val <= x["Monate"][1]:
                zeit = datetime.time(val.hour, val.minute)
                if x["Tageszeit"][0] <= zeit <= x["Tageszeit"][1]:
                    reserve[i] = x["Wert"]
                elif (x["Tageszeit"][0] > x["Tageszeit"][1]) and (
                    (x["Tageszeit"][0] <= zeit <= datetime.time(23, 59))
                    or (datetime.time(0) <= zeit <= x["Tageszeit"][1])
                ):
                    reserve[i] = x["Wert"]

    return reserve
