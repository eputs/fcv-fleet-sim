#!/usr/bin/env python

# Import packages
from matplotlib import pyplot as plt
import datetime

# from Simu_Waerme import calcWaterTemperature
from Simu_Klassen import *
from parameter_base_case import *
import photovoltaik as pv

from pathlib import Path
import os
import pickle

EXPORT_PATH = Path(os.path.abspath(__file__)).parents[1].joinpath('export')


P_pv = np.array([])
dictSimParam = {
    "Batterie Ladeleistung [W]": [],
    "Batterie Kapazität [kWh]": [],
    "Reserve Sommer [kWh]": [],
    "Reserve Winter [kWh]": [],
    "Elektrolyseur Leistung [W]": [],
    "Elektrolyseur Betriebszeit [Tage]": [],
    "Preis Strombezug [€/kWh]": [],
    "Preis Stromverkauf [€/kWh]": [],
    "Preis Wasserstoff [€/kWh]": [],
    "Energiebedarf [kWh]": [],
    "Energiedefizit [kWh]": [],
    "Energieüberschuss [kWh]": [],
    "Energie aus Batterie [kWh]": [],
    "Energie in Batterie [kWh]": [],
    "Energie in Elektrolyseur [kWh]": [],
    "Energie Wasserstoff [kWh]": [],
    "Energie Wassertank [kWh]": [],
    "Energie Netzbezug [kWh]": [],
    "Energie Netzeinspeisung [kWh]": [],
    "Kosten Batterie [€]": [],
    "Kosten Wasserstoffsystem [€]": [],
    "Kosten Strombezug [€]": [],
    "Erlös Stromverkauf [€]": [],
    "Erlös Wasserstoffverkauf [€]": [],
    "Gesamtkosten ohne Speicher [€]": [],
    "Gesamtkosten mit Speichern [€]": [],
    "Kosteneinsparung Speicher [€]": [],
    "Kosteneinsparung Sektorkopplung [€]": [],
    "Gesamtkosteneinsparung [€]": [],
    "CO2 Speicher [kg]": [],
    "CO2 Sektorkopplung [kg]": [],
    "CO2 Gesamt [kg]": [],
    "CO2 Kennzahl [kgCO2/kWh]": [],
    "CO2 Kennzahl ohne Speicher [kgCO2/kWh]": [],
}


def simulateObjects(
    battery,
    electrolyser,
    fuellcell,
    hydrogentank,
    grid,
    watertank,
    Lastprofilnummer,
    batteryReserveArray,
    summerReserve,
    winterReserve,
    visualize,
    visualize_pickle=True
):
    global P_pv
    global dictSimParam

    # visualize = False  # Übergeben?

    # Differenzleistung berechnen
    P_verbraucher = Lastprofile[:, Lastprofilnummer]
    energyRequirement = jahresenergie_kWh(P_verbraucher)

    # if P_pv is empty get values once, then reuse values
    if P_pv.size == 0:
        # P_pv = pv.calculate_power(G_h, z, P_peak, PR, G_stc, v, inclination)

        pv_model = Photovoltaic(P_peak, PR, v, inclination)
        P_pv = pv_model.get_results()
        P_pv = [power for power in P_pv for i in range(int(525600/len(P_pv)))]
               # Repeat each entry `n` times, since the PV results are in `n`
               # minute intervals.

    P_dif = P_pv - P_verbraucher
    P_dif_before = np.copy(P_dif)

    # ohne Speichersystem
    P_def_None = negval_cutoff(-P_dif_before)
    P_ueber_None = negval_cutoff(P_dif_before)
    E_def_None = jahresenergie_kWh(P_def_None)
    E_ueber_None = jahresenergie_kWh(P_ueber_None)

    # Zeitspanne festlegen
    # Es muss immer 1 Jahr simuliert werden
    minutes = np.arange(
        datetime.datetime(2020, 1, 1),
        datetime.datetime(2020, 12, 31),
        datetime.timedelta(minutes=1),
    ).astype(datetime.datetime)

    # Arrays initialisieren
    batterySOC = np.zeros(len(minutes))
    hydrogentankLevel = np.zeros(len(minutes))
    hydrogentankEnergy = np.zeros(len(minutes))
    batteryPower = np.zeros(len(minutes))
    batteryEnergy = np.zeros(len(minutes))
    batteryEnergyOut = np.zeros(len(minutes))
    elektrolyserPower = np.zeros(len(minutes))
    energyMarketPower = np.zeros(len(minutes))
    T_Wasser = np.zeros(len(minutes))

    # Zeitmessung starten
    start = datetime.datetime.now()

    # Beginn REGELSTRATEGIE ############################################################
    elektrolyserOn = False  # Elektrolyseur ist zu Beginn aus

    for i, t in enumerate(minutes):

        # ELEKTROLYSER############################################################
        # Wenn Batterie an der Reservegrenze ist wird zB Föhnen trotzdem mit Batterieentladung zugelassen, Elektrolyseurbetrieb aber nicht.
        # Elektrolyseur wird über die Batteriereserve gesteuert.
        # Es kann sein, dass P_dif positiv in den Elektrolyseur reingeht aber negativ rauskommt und durch Batterieentladung gedeckt werden muss.

        if battery.SOC >= (batteryReserveArray[i] + 5) and not elektrolyserOn:
            # Einschaltgrenze für den Elektrolyseur 5% höher als Batteriereserve
            # Erforderlich um hochfrequentes (minütlich) ein und ausschalten von Elektrolyseur zu vermeiden,
            # wenn die Batterie geladen wird und im Bereich der Grenze ist
            elektrolyserOn = True

        if battery.SOC >= batteryReserveArray[i] and elektrolyserOn:
            # Elektrolyseur an bis Batteriereserve erreicht ist
            P_dif[i] -= electrolyser.maxPower
            electrolyser.powerIn(electrolyser.maxPower)
        else:
            # Elektrolyseur ausschalten bzw. aus lassen
            electrolyser.powerIn(0)
            elektrolyserOn = False

        # BATTERY##################################################################
        battery.powerIn(P_dif[i], 0)  # Batterie wird entladen
        P_dif[i] -= battery.power

        # GRID#####################################################################
        # Den Rest einspeisen oder kaufen
        grid.powerIn(P_dif[i])

        # LEVEL Tracking###########################################################
        batterySOC[i] = battery.SOC
        batteryEnergy[i] = battery.energy
        batteryEnergyOut[i] = battery.energyOut
        batteryPower[i] = battery.power
        elektrolyserPower[i] = electrolyser.power
        hydrogentankLevel[i] = hydrogentank.hydrogentankLevel
        hydrogentankEnergy[i] = hydrogentank.energy
        energyMarketPower[i] = P_dif[i]

    # Ende REGELSTRATEGIE ############################################################

    # Anbindung Sektorkopplung #######################################################
    T_Wasser = watertank.calcWaterTemperature(elektrolyserPower)
    storedHeatEnergy = watertank.storedEnergy
    additionalHeatEnergy = watertank.additionalEnergy
    loadEnergy = watertank.loadEnergy

    # print("Stored Heatenergy: ", storedHeatEnergy, "kWh")
    # print("Additional Heatenergy: ", additionalHeatEnergy / 1000, "kWh")
    # print("needed Heatenergy: ", loadEnergy / 1000, "kWh")

    # Ökonomie #######################################################
    costWithoutStorage = abs(E_def_None * grid.electricityPrice) - abs(
        E_ueber_None * grid.feedInTariff
    )

    costsEnergyDemand = abs(grid.energyDemand * grid.electricityPrice)
    proceedsEnergyFeedIn = abs(grid.energyFeedIn * grid.feedInTariff)
    proceedsHydrogenSale = abs(hydrogentank.energy * hydrogentank.hydrogenPrice)
    costsElektrolyser = abs(electrolyser.maxPower * electrolyser.systemCost)
    costsBattery = abs(battery.maxEnergy * battery.systemCost)

    costWithStorage = (
        costsEnergyDemand
        - proceedsEnergyFeedIn
        - proceedsHydrogenSale
        + costsElektrolyser
        + costsBattery
    )

    costSaving = costWithoutStorage - costWithStorage
    costSavingHeat = storedHeatEnergy * K_waerme
    costSavingTotal = costSaving + costSavingHeat

    # Ökologie #######################################################
    # Berechne CO2c_net
    CO2proJahr = (
        (abs(grid.energyDemand) * grid.CO2perkWh)
        + (battery.CO2_KgperkWh * battery.maxEnergy / 20)
        + (electrolyser.CO2_KgperkWh * electrolyser.maxPower / 1000 / 20)
        - (Dampfreformierung_CO2 * hydrogentank.energy)
    )
    CO2Heat = Waerme_CO2 * storedHeatEnergy
    CO2Gesamt = CO2proJahr - CO2Heat
    CO2prokWh = CO2proJahr / jahresenergie_kWh(P_verbraucher)
    CO2WithoutStorage = (abs(E_def_None * grid.CO2perkWh)) / jahresenergie_kWh(
        P_verbraucher
    )

    # print("Simulationsdauer: ", datetime.datetime.now() - start, " s")
    # print(f"Batteriegröße: {battery.maxEnergy}kWh, Elektroyseurleistung: {electrolyser.maxPower}W")

    # Dictionary mit allen Simulationsparametern befüllen##################################

    dictSimParam["Batterie Ladeleistung [W]"].append(battery.maxPower)
    dictSimParam["Batterie Kapazität [kWh]"].append(battery.maxEnergy)
    dictSimParam["Reserve Sommer [kWh]"].append(
        battery.maxEnergy * (summerReserve) / 100
    )
    dictSimParam["Reserve Winter [kWh]"].append(
        battery.maxEnergy * (winterReserve) / 100
    )
    dictSimParam["Elektrolyseur Leistung [W]"].append(electrolyser.maxPower)
    dictSimParam["Elektrolyseur Betriebszeit [Tage]"].append(
        (electrolyser.minutesTurnedOn / 60) / 24
    )
    dictSimParam["Preis Strombezug [€/kWh]"].append(grid.electricityPrice)
    dictSimParam["Preis Stromverkauf [€/kWh]"].append(grid.feedInTariff)
    dictSimParam["Preis Wasserstoff [€/kWh]"].append(hydrogentank.hydrogenPrice)
    dictSimParam["Energiebedarf [kWh]"].append(energyRequirement)
    dictSimParam["Energiedefizit [kWh]"].append(E_def_None)
    dictSimParam["Energieüberschuss [kWh]"].append(E_ueber_None)
    dictSimParam["Energie aus Batterie [kWh]"].append(battery.energyOut)
    dictSimParam["Energie in Batterie [kWh]"].append(battery.energyIn)
    dictSimParam["Energie in Elektrolyseur [kWh]"].append(electrolyser.energyIn)
    dictSimParam["Energie Wasserstoff [kWh]"].append(hydrogentank.energy)
    dictSimParam["Energie Wassertank [kWh]"].append(storedHeatEnergy)
    dictSimParam["Energie Netzbezug [kWh]"].append(abs(grid.energyDemand))
    dictSimParam["Energie Netzeinspeisung [kWh]"].append(grid.energyFeedIn)
    dictSimParam["Kosten Batterie [€]"].append(costsBattery)
    dictSimParam["Kosten Wasserstoffsystem [€]"].append(costsElektrolyser)
    dictSimParam["Kosten Strombezug [€]"].append(costsEnergyDemand)
    dictSimParam["Erlös Stromverkauf [€]"].append(proceedsEnergyFeedIn)
    dictSimParam["Erlös Wasserstoffverkauf [€]"].append(proceedsHydrogenSale)
    dictSimParam["Gesamtkosten ohne Speicher [€]"].append(costWithoutStorage)
    dictSimParam["Gesamtkosten mit Speichern [€]"].append(costWithStorage)
    dictSimParam["Kosteneinsparung Speicher [€]"].append(costSaving)
    dictSimParam["Kosteneinsparung Sektorkopplung [€]"].append(costSavingHeat)
    dictSimParam["Gesamtkosteneinsparung [€]"].append(costSavingTotal)
    dictSimParam["CO2 Speicher [kg]"].append(CO2proJahr)
    dictSimParam["CO2 Sektorkopplung [kg]"].append(CO2Heat)
    dictSimParam["CO2 Gesamt [kg]"].append(CO2Gesamt)
    dictSimParam["CO2 Kennzahl [kgCO2/kWh]"].append(CO2prokWh)
    dictSimParam["CO2 Kennzahl ohne Speicher [kgCO2/kWh]"].append(CO2WithoutStorage)

    #####################################################################################

    # VISUALISIERUNG###############################################################

    if visualize:

        plt.rcParams["axes.grid"] = True

        # Systemspeicherstände
        plt.figure(figsize=(9, 6))
        plt.suptitle("Systemspeicherstände")

        ax1 = plt.subplot(2, 1, 1)
        ax1.set_title("Batterie SOC")
        ax1.set_ylabel("SOC [%]")
        ax1.plot(minutes, batterySOC)

        ax4 = plt.subplot(2, 1, 2, sharex=ax1)
        ax4.set_title("erzeugte Wasserstoffmenge")
        ax4.set_ylabel("H2 [kWh]")
        ax4.plot(minutes, hydrogentankEnergy)

        filename = f"Systemspeicherstände_{datetime.datetime.now().strftime('%Y_%m_%d_%H_%M_%S')}"
        path = EXPORT_PATH.joinpath(f'{filename}.png')
        plt.savefig(path)
        if visualize_pickle:
            path = EXPORT_PATH.joinpath(f'{filename}.fig.pickle')
            pickle.dump(plt.gcf(), open(path, 'wb'))

        ########################################################################
        # Leistungen
        data = {
            "Elektrolyseurleistung": elektrolyserPower,
            "Batterieleistung": batteryPower,
            "Netzleistung": energyMarketPower,
        }

        plt.figure(figsize=(9, 6))
        plt.suptitle("Leistungsverläufe")

        ax1 = plt.subplot(2, 1, 1)
        ax1.stackplot(minutes, data.values(), labels=data.keys(), alpha=0.8)
        ax1.legend(loc="upper left")
        ax1.set_title("Leistungsverläufe gestackt")
        ax1.set_ylabel("Leistung [W]")

        ax2 = plt.subplot(2, 1, 2, sharex=ax1)
        ax2.set_title("Leistungsverläufe")
        ax2.set_ylabel("Leistung [W]")
        ax2.set_xlabel("Zeit")
        ax2.plot(minutes, P_dif_before, label="Differenzleistung P_pv-P_last")
        ax2.plot(minutes, elektrolyserPower, label="Elektrolyseurleistung")
        ax2.plot(minutes, batteryPower, label="Batterieleistung")
        ax2.plot(minutes, energyMarketPower, label="Netzleistung")
        ax2.legend(loc="upper left")
        # ax2.plot(minutes, data.values(), labels=data.keys(),)

        filename = f"Leistungen_{datetime.datetime.now().strftime('%Y_%m_%d_%H_%M_%S')}"
        path = EXPORT_PATH.joinpath(f'{filename}.png')
        plt.savefig(path)
        if visualize_pickle:
            path = EXPORT_PATH.joinpath(f'{filename}.fig.pickle')
            pickle.dump(plt.gcf(), open(path, 'wb'))

        ########################################################################
        # Leistungsverläufe und Batterie

        plt.figure(figsize=(9, 6))
        plt.suptitle("Leistungsverläufe und Batterie SOC")

        ax1 = plt.subplot(2, 1, 1)
        ax1.set_title("Batterie SOC")
        ax1.set_ylabel("Batterie SOC [%]")
        ax1.plot(minutes, batterySOC)

        ax2 = plt.subplot(2, 1, 2, sharex=ax1)
        ax2.set_title("Leistungsverläufe")
        ax2.set_ylabel("Leistung [W]")
        ax2.set_xlabel("Zeit")
        ax2.plot(minutes, P_dif_before, label="Differenzleistung P_pv-P_last")
        ax2.plot(minutes, elektrolyserPower, label="Elektrolyseurleistung")
        ax2.plot(minutes, batteryPower, label="Batterieleistung")
        ax2.plot(minutes, energyMarketPower, label="Netzleistung")
        ax2.legend(loc="upper left")

        filename = f"Leistungsverläufe_und_Batterie_{datetime.datetime.now().strftime('%Y_%m_%d_%H_%M_%S')}"
        path = EXPORT_PATH.joinpath(f'{filename}.png')
        plt.savefig(path)
        if visualize_pickle:
            path = EXPORT_PATH.joinpath(f'{filename}.fig.pickle')
            pickle.dump(plt.gcf(), open(path, 'wb'))

        ##########################################################################
        # Überprüfung Batteriereserve

        plt.figure(figsize=(9, 6))
        plt.suptitle("Betriebszeiten Elektrolyseur")

        ax1 = plt.subplot(3, 1, 1)
        ax1.set_title("Batteriereserve")
        ax1.set_ylabel("Batteriereserve [%]")
        ax1.plot(minutes, batteryReserveArray)

        ax2 = plt.subplot(3, 1, 2, sharex=ax1)
        ax2.set_title("Batterie SOC")
        ax2.set_ylabel("SOC [%]")
        ax2.plot(minutes, batterySOC)

        ax3 = plt.subplot(3, 1, 3, sharex=ax1)
        ax3.set_title("Elektrolyseurleistung")
        ax3.set_ylabel("Leistung [W]")
        ax3.set_xlabel("Zeit")
        ax3.plot(minutes, elektrolyserPower)

        filename = f"Überprüfung_Batteriereserve_{datetime.datetime.now().strftime('%Y_%m_%d_%H_%M_%S')}"
        path = EXPORT_PATH.joinpath(f'{filename}.png')
        plt.savefig(path)
        if visualize_pickle:
            path = EXPORT_PATH.joinpath(f'{filename}.fig.pickle')
            pickle.dump(plt.gcf(), open(path, 'wb'))

        plt.show()

    ##########################################################################

    # Rückgabe der Simulationsparameter
    return dictSimParam


if __name__ == "__main__":
    print("Bitte Skript Kennzahlen.py ausführen")
