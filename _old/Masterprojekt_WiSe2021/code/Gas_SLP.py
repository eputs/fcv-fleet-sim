#!/usr/bin/env python

import os
import csv
import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d

from own_utility_functions import *

currentDirectory = os.getcwd()
# directory_data = find_directory("/_data/")
# print(directory_data)

# SLP-Paramter:
A = 2.72605290290905
B = -34.6507
C = 7.52364
D = 0.07969
T_0 = 40

# Energiebedarf:
# https://www.thermondo.de/info/rat/heizen/energieverbrauch-durchschnittsfamilie/
Energiefaktor = 100  # [kWh/a/m^2]
Wohnflaeche = 140  # [m^2]
Q_a = Energiefaktor * Wohnflaeche  # [kWh/a]

path = os.path.abspath(
    os.path.join(os.path.dirname(__file__), "..", "input/UNNA_stuendlich_2019.csv")
)

with open(path) as csvdatei:
    csv_reader_object = csv.reader(csvdatei, delimiter=";")
    T_UNNA_h_2019_meta = np.empty((0, 4), dtype=int)
    T_UNNA_h_2019 = np.empty((0), dtype=int)
    cnt = 0
    for row in csv_reader_object:
        if cnt > 0:
            T_UNNA_h_2019_meta = np.append(
                T_UNNA_h_2019_meta,
                [[int(row[0][0:4]), int(row[0][4:6]), int(row[0][6:8]), int(row[1])]],
                axis=0,
            )
            T_UNNA_h_2019 = np.append(T_UNNA_h_2019, [int(row[1])])
        cnt += 1

mid_temp = 0
T_UNNA_d_2019 = np.empty((0), dtype=float)
cnt = 0
for i in range(T_UNNA_h_2019.shape[0]):
    mid_temp += T_UNNA_h_2019[i]
    cnt += 1
    if cnt == 24:
        T_UNNA_d_2019 = np.append(T_UNNA_d_2019, [mid_temp / 240.0])
        mid_temp = 0
        cnt = 0

h_t = []
for i in range(T_UNNA_d_2019.shape[0]):
    h = (A / (1 + (B / (T_UNNA_d_2019[i] - T_0)) ** C)) + D
    # print(h)
    h_t = np.append(h_t, [h])

# print(h_t)
# print(h_t.max())
# print(h_t.min())

path2 = os.path.abspath(
    os.path.join(os.path.dirname(__file__), "..", "input/Lastprofil_Unna.csv")
)

with open(path2) as csvdatei:
    csv_reader_object = csv.reader(csvdatei, delimiter=";")
    Loadprofile_table = np.empty((10, 24))
    row_cnt = 0
    for row in csv_reader_object:
        for columns in range(24):
            Loadprofile_table[row_cnt, columns] = float(row[columns])
        row_cnt += 1
# print(Loadprofile_table)

temprange = 0
Loadprofile_factor = np.empty(T_UNNA_d_2019.shape[0] * 24)
for days in range(T_UNNA_d_2019.shape[0]):
    daytemp = T_UNNA_d_2019[days]
    if daytemp >= 25:
        temprange = 9
    elif daytemp >= 20:
        temprange = 8
    elif daytemp >= 15:
        temprange = 7
    elif daytemp >= 10:
        temprange = 6
    elif daytemp >= 5:
        temprange = 5
    elif daytemp >= 0:
        temprange = 4
    elif daytemp >= -5:
        temprange = 3
    elif daytemp >= -10:
        temprange = 2
    elif daytemp >= -15:
        temprange = 1
    else:
        temprange = 0
    for time in range(24):
        # Loadprofile_factor[days*24+time]=days*24+time
        Loadprofile_factor[days * 24 + time] = Loadprofile_table[temprange, time]

# print(Loadprofile_factor)
# x=364
# for i in range(x*24,x*24+24):
# plt.plot(Loadprofile_factor[x*24:x*24+24])
# plt.show()

Q_d = np.empty(365, dtype=float)
Q_h = np.empty(365 * 24, dtype=float)
for days in range(365):
    Q_d[days] = Q_a / 365.0 * h_t[days]
    for time in range(24):
        Q_h[days * 24 + time] = Loadprofile_factor[days * 24 + time] / 100 * Q_d[days]

# MinutenWerte nicht interpoliert
# Q_1min = np.empty(365*24*60, dtype=float)
# for h in range(24*365):
#    for m in range(60):
#        Q_1min[h*60+m] = Q_h[h]

f = interp1d(np.arange(0, np.size(Q_h)), Q_h, kind="cubic")
Q_1min = f(np.arange(0, np.size(Q_h) - 1, 1 / 60))
Q_1min = np.append(Q_1min, np.empty(60))
# durch splines entstehen negtive Werte. Diese dürfen nicht vorkommen und werden durch 0 ersetzt, da es die späteren Berechnungen erleichtert
for i, value in enumerate(Q_1min):
    if value < 0:
        Q_1min[i] = 0

# np.save(directory_data+'Lastprofil_Waerme.npy', Q_1min)

# print(Q_d[0])
# print(h_t[0])
# plt.figure(1)
# plt.plot(Q_h[0:24*3])
# plt.figure(2)
# plt.plot(Q_d)
# plt.figure(3)
# plt.plot(Q_1min[0:24*3*60])
# plt.show()

# c_wasser = 4189
# V_Wasser = 400
# T_Wasser = np.empty(24*60*365, dtype=float)
# T_Wasser[0] = 60
# for d in range(0,364):
#    for h in range(0,24*60):
#        i = d*24*60+h
#        T_Wasser[i+1]=(T_Wasser[i]-(Q_1min[i]*1000*60-47.79*1000*60/24)/(V_Wasser*c_wasser))    #-Wohnflaeche*Energiefaktor/365*1000*60/24)/(V_Wasser*c_wasser))
#    T_Wasser[i+1]=60

# plt.figure(4)
# plt.plot(T_Wasser[0:24*60*1-1])

# plt.show()
