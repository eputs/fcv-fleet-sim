#!/usr/bin/env python

from math import sqrt
import numpy as np
import pandas as pd
import Gas_SLP
import PySAM.BatteryStateful as bs
import PySAM.Pvwattsv8 as pv
from pathlib import Path
import json
import logging as log
import math
import typing as t

import parameter_base_case as pbc

batt_config_path = Path(__file__).parent.joinpath('model_parameters',
    'battery_stateful.json')
pv_config_path = Path(__file__).parent.joinpath('model_parameters',
    'pvwattsv8.json')


_ENERGY_IN_H2 = 33  # kWh/kg  # TODO Find citation.

class Battery:

    def __init__(
            self, maxPower=None, maxEnergy=None,
            energy=None,  # Note: Energy is deprecated.
            efficiency=None,  # Note: efficiency is deprecated.
            systemCost: float = pbc.K_system_batterie,
            CO2_KgperkWh: float = pbc.Batterie_CO2):

        # Loading the model parameters from the configuration file.
        self.model = bs.new()
        with open(batt_config_path, 'r') as config_file:
            data = json.load(config_file)
            for key, val in data.items():
                if key != 'number_inputs' and key != '__':
                    self.model.value(key, val)

        if maxPower is not None:
            self.maxPower = maxPower  # In Watts
        else:
            self.maxPower = min(self.model.StatePack.P_chargeable * 1000,
                                self.model.StatePack.P_dischargeable * 1000)

        # if maxEnergy is not None:
        #     self.model.value('nominal_energy', maxEnergy)
        # if energy is not None:
            # self.model.value('initial_SOC',
            #                  energy / self.model.value('nominal_energy') * 100)
        # if efficiency is not None:
        #     log.error('Efficiency is deprecated. Use more detailed battery ' +
        #               'modelling parameters.')
        #     input('Press enter to ignore the efficiency parameter and ' +
        #           'continue... Else, press <Ctrl> + C to quit the program.')

        self.model.value('dt_hr', 1 / 60)  # Run the simulation for a one-minute timestep.
        self.model.setup()

        self.maxEnergy = self.model.ParamsPack.nominal_energy
        self.voltage = self.model.ParamsPack.nominal_voltage
        self.systemCost = systemCost
        self.CO2_KgperkWh = CO2_KgperkWh

        self.SOC = self.model.StatePack.SOC
        self.energy = self.SOC * self.maxEnergy
                      # The current energy level of the battery.
        self.power = self.model.StatePack.P * 1000
        self.energyIn = 0
        self.energyOut = 0
        self.powerLeftover = 0 * 1000

    def powerIn(self, P, reserve=None):

        # TODO Reserve parameter is deprecated.

        # In elysim, if P<0, it means that the system is trying to draw power
        # from  the battery. If P>0, it means that the system is trying to
        # charge the battery with excess (solar) power that is available. This is
        # oppisite to the SAM model's convention. Therefore, we make P = -P. We
        # will also invert the output values of the model. Also, ElySim's power
        # is in W, while SAM expects it in kW.

        # Limit the input power based on the maxPower parameter.
        P_in = P  # Make a backup of the original input power, before capping
                  # it at maxPower.
        if abs(P) > self.maxPower:
            P = self.maxPower

        P = -P/1000  # To kW.

        # Save a copy of the energy of the battery before the power is fed in.
        self.prev_energy = self.model.StatePack.V * self.model.StatePack.Q / 1000

        # Execute the timestep!
        self.model.value('input_power', P)
        self.model.execute(1)

        # Revert power to ElySimulation's understanding.
        P = -P * 1000

        # Extract the results important to us.
        self.energy = self.model.StatePack.V * self.model.StatePack.Q / 1000
                      # The current energy level of the battery in kWh.
        # self.SOC = self.energy / self.maxEnergy * 100
        self.SOC = self.model.StatePack.SOC
                   # The current energy level of the battery as a percentage.
        self.power = -self.model.StatePack.P * 1000
                     # The power charged into the battery, expressed in W.

        if self.prev_energy >= self.energy:
            self.energyOut = abs(self.prev_energy - self.energy)
        else:
            self.energyIn = abs(self.prev_energy - self.energy)

        self.powerLeftover = P_in - self.power


class Battery_Old:

    def __init__(
        self, maxPower, maxEnergy, energy, efficiency, systemCost, CO2_KgperkWh
    ):
        self.maxPower = maxPower
        self.maxEnergy = maxEnergy
        self.energy = energy
        self.efficiency = efficiency
        self.systemCost = systemCost
        self.CO2_KgperkWh = CO2_KgperkWh
        self.SOC = 0
        self.power = 0
        self.energyIn = 0
        self.energyOut = 0
        self.powerLeftover = 0

    def powerIn(self, P, reserve):

        if abs(P) <= self.maxPower:
            # when smaller than maximum charge rate
            self.power = P
        else:
            # else charge at maximum battery rate
            if P < 0:
                self.power = -self.maxPower
            else:
                self.power = self.maxPower

        # integrate power over time --> energy
        if P >= 0:
            # multiply efficiency when charging
            newEnergie = self.energy + (self.power * sqrt(self.efficiency)) / 60 / 1000
        else:
            # divide efficiency when discharging
            newEnergie = self.energy + (self.power / sqrt(self.efficiency)) / 60 / 1000

        # dont charge or discharge when full or empty
        if (newEnergie <= self.maxEnergy and newEnergie >= reserve) or (
            newEnergie < reserve and newEnergie > 0
        ):

            if newEnergie > self.energy:
                # self.energyIn = self.energyIn + newEnergie - self.energy
                self.energyIn = self.energyIn + (newEnergie - self.energy) / sqrt(
                    self.efficiency
                )
            else:
                # self.energyOut = self.energyOut + self.energy - newEnergie
                self.energyOut = self.energyOut + (self.energy - newEnergie) * sqrt(
                    self.efficiency
                )
            self.energy = newEnergie

        else:
            self.power = 0
            # self.energy = E_bat_old
        try:
            self.SOC = (self.energy / self.maxEnergy) * 100
        except:
            self.SOC = 0
        self.powerLeftover = P - self.power


class FuelcellVehicle:

    def __init__(self, distances:pd.Series, vehicle_id):  # , model_parameters: dict):
        """
        Initialise the model parameters{TODO::, using a SUMO typ.xml file}.

        The distances dataframe must have two columns. The first column with
        the various dates, and the second column with the distances travelled
        on those dates.
        """

        # Take this from model_parameters instead.
        self.vehicle_id = vehicle_id
        self.energy_efficiency = 0.6  # kWh/km
        self.fuelcell_efficiency = 0.7  # 70 %
        self.base_cost = 60000  # €
        self.tank = _Tank()
        self.distances = distances
        self.energies: pd.DataFrame = None

    def calc_energies(self) -> pd.DataFrame:
        """ Calculate energy required to move the vehicle x kms per day. """

        self.energies = pd.DataFrame(self.distances)
        self.energies.columns = ['distance']
        self.energies['electrical_energy'] = self.energy_efficiency * self.energies['distance']
        self.energies['usable_energy'] = self.energies['electrical_energy'] / self.fuelcell_efficiency
        self.energies['energy'] = self.energies['usable_energy'].map(self.tank.calc_energy)

        self.mean_energy = self.energies['energy'].mean()

        return self.energies

    def calc_capex(self) -> float:

        return self.base_cost


class _Tank:
    """Keeps track of the energy in the vehicle."""

    def __init__(self):

        # Get this from model parameter file.
        self.nom_pressure = 700  # Bar
        self.energy_efficiency = 0.95  # 95%

    def calc_energy(self, usable_energy) -> float:

        """ Calculate the energy loss in the tank (due to daily pressure
        leaks?) """

        self.usable_energy = usable_energy
        self.energy = usable_energy / self.energy_efficiency
            # Energy stored in tank.
        return self.energy


class Compressor:
    """Pressurizes the tank, and reports how much energy was lost in the
       process."""

    def __init__(self, daily_hydrogen_energy: pd.DataFrame):

        self.compression_energy = 0.10  # 10% of the Hydrogen energy is
                                        # required for compression.
        self.cost_rate = 8000  # EUR/kW This is quite high! Can it be true??
        self.daily_hydrogen_energy = daily_hydrogen_energy

    def calc_energy(self) -> float:

        """Calculate the energy loss due to pressurizing the tank at
           its nominal pressure."""  # TODO

        self.energy_cost = self.daily_hydrogen_energy * self.compression_energy

        return self.energy_cost

    def calc_capex(self) -> float:
        self.avg_power = self.energy_cost.sum(1).mean() / 24
            # Assuming constant operation.
        self.capex = self.avg_power * self.cost_rate
        return self.capex

class Electrolyser:
    def __init__(self, daily_hydrogen_energy: pd.DataFrame,
                 efficiency = 0.75, systemCost = 4000, CO2 = 30):
        # TODO Read these model paramters from configuration files.
        self.daily_hydrogen_energy = daily_hydrogen_energy
        self.efficiency = efficiency
        self.cost_rate = systemCost  # EUR/kW
        self.co2_rate = CO2  # kg/kW of Electrolyser power.

        # TODO Delete these variables? I don't think I need them anymore.
        # self.power = 0
        # self.energyIn = 0
        # self.minutesTurnedOn = 0

    def calc_energy(self) -> float:
        self.energy = self.daily_hydrogen_energy / self.efficiency
        return self.energy

    def calc_capex(self) -> float:
        # first we need to calculate the size of the electrolyser.
        self.avg_power = self.energy.sum(1).mean() / 24
            # Assuming that the electrolyser is on the whole day.
            # energy[kWh] / time[h] = power[kW]
        self.capex = self.avg_power * self.cost_rate
        return self.capex

    def calc_co2(self) -> float:

        self.co2 = self.power * self.co2_rate
        return self.co2


# TODO Delete this class? Not useful for macrosimulation...
class H2:
    def __init__(self, maxEnergy, hydrogenPrice):
        self.maxEnergy = maxEnergy  # Maximum energy that can be stored in the
                                    # tank.
        self.hydrogenPrice = hydrogenPrice
        self.energy = 0
        self.hydrogentankLevel = 0

    def createElectrolyser(self, maxPower, efficiency, systemCost, CO2_KgperkWh):
        return H2.Electrolyser(self, maxPower, efficiency, systemCost, CO2_KgperkWh)

    def createFuelcell(self, maxPower, efficiency):
        return H2.Fuelcell(self, maxPower, efficiency)

    class Electrolyser:
        def __init__(self, outer, maxPower, efficiency, systemCost, CO2_KgperkWh):
            self.outer = outer
            self.maxPower = maxPower
            self.efficiency = efficiency
            self.systemCost = systemCost
            self.CO2_KgperkWh = CO2_KgperkWh
            self.power = 0
            self.energyIn = 0
            self.minutesTurnedOn = 0

        def powerIn(self, P):
            """ Calculates the ammount of energy of Hydrogen generated, if the
                electrolyser is run at power, P, for 1 minute. """

            if P >= 0:
                if P > 0:
                    self.minutesTurnedOn += 1
                if P <= self.maxPower:
                    self.power = P
                else:
                    self.power = self.maxPower

                newEnergy = (
                    self.outer.energy + (self.power * self.efficiency) / 60 / 1000
                )

                if newEnergy <= self.outer.maxEnergy and newEnergy >= 0:
                    self.outer.energy = newEnergy
                else:
                    self.power = 0
                self.energyIn = self.energyIn + self.power / 60 / 1000
                self.outer.hydrogentankLevel = (
                    self.outer.energy / self.outer.maxEnergy
                ) * 100

    class Fuelcell:
        def __init__(self, outer, maxPower, efficiency):
            self.outer = outer
            self.maxPower = maxPower
            self.efficiency = efficiency
            self.power = 0

        def powerIn(self, P):
            if P <= 0:
                if -P <= self.maxPower:
                    self.power = P
                else:
                    self.power = self.maxPower

                newEnergy = (
                    self.outer.energy + (self.power / self.efficiency) / 60 / 1000
                )

                if newEnergy <= self.outer.maxEnergy and newEnergy >= 0:
                    self.outer.energy = newEnergy
                else:
                    self.power = 0

                self.outer.hydrogentankLevel = (
                    self.outer.energy / self.outer.maxEnergy
                ) * 100


class Grid:
    def __init__(self, electricityPrice, feedInTariff, CO2perkWh):
        self.electricityPrice = electricityPrice
        self.feedInTariff = feedInTariff
        self.CO2perkWh = CO2perkWh
        self.energyDemand = 0
        self.energyFeedIn = 0

    def powerIn(self, P):

        # integrate power over time --> energy
        if P >= 0:
            self.energyFeedIn = self.energyFeedIn + (P / 60 / 1000)
        else:
            self.energyDemand = self.energyDemand + (P / 60 / 1000)


class Watertank: #notes: efficiency betrachten
    def __init__(
        self, volume, efficiency
    ):
        self.volume = volume
        self.efficiency = efficiency
        self.flowPump = 40      #[l/min]
        self.k_wt = 2000        #[W/(m^2*K)]
        self.A_wt = 0.1         #[m^2]
        self.c_wasser = 4183    #[J/(kg*K)]
        self.T_H2 = 80          #[°C]
        self.additionalHeater = 15000   #[W]
        self.additionalHeaterEn = 0     #[bool]
        self.additionalHeaterMin = 55   #[°C]
        self.additionalHeaterMax = 65   #[°C]
        self.waterTempStart = 50        #[°C]
        self.storedEnergy = 0
        self.additionalEnergy = 0
        self.loadEnergy = 0

    def calcTransferedHeat(T_in, T_H2, V_Pumpe, k_wt, A_wt, c_wasser, P_Input):
        dQ=k_wt*A_wt*(T_H2-T_in)/(1+k_wt*A_wt/(c_wasser*V_Pumpe/60))
        if dQ>P_Input:
            return P_Input
        else:
            return dQ

    def calcWaterTemperature(self, P_Input_Wasser):
        T_wasser = np.zeros(len(P_Input_Wasser))
        T_wasser[0]= self.waterTempStart

        for i in range(len(P_Input_Wasser)-1):      #über P_Input_Wasser Array iterrieren
            dQTrans = Watertank.calcTransferedHeat(T_wasser[i], self.T_H2, self.flowPump, self.k_wt, self.A_wt, self.c_wasser, P_Input_Wasser[i]*self.efficiency)
            if (T_wasser[i] < self.additionalHeaterMin) and (self.additionalHeaterEn == 0):
                self.additionalHeaterEn = 1
            if (T_wasser[i] > self.additionalHeaterMax) and (self.additionalHeaterEn == 1):
                self.additionalHeaterEn = 0

            self.storedEnergy += dQTrans
            self.additionalEnergy += self.additionalHeaterEn * self.additionalHeater
            self.loadEnergy += Gas_SLP.Q_1min[i] * 1000
            dQ = dQTrans + self.additionalHeaterEn * self.additionalHeater - Gas_SLP.Q_1min[i] * 1000
            T_wasser[i+1]=(T_wasser[i]+dQ*60/(self.volume*self.c_wasser))
        self.storedEnergy /= 60 * 1000
        self.additionalEnergy /= 60
        self.loadEnergy /= 60
        return T_wasser


class Photovoltaic:
    def __init__(self, P_peak: int = None, PR: int = None, v: int = None,
                 inclination: int = None, E_desired: int = None):

        self.model = pv.default("PVWattsNone")

        # Loading the model parameters from the configuration file.
        with open(pv_config_path, 'r') as config_file:
            data = json.load(config_file)
            for key, val in data.items():
                if key != 'number_inputs' and key != '__':
                    self.model.value(key, val)

        self.model.value("solar_resource_file",
                         str(Path(__file__).parent.joinpath(
                            '..', 'input', 'stuttgart_msg-iodc_15_2019.csv')))

        # Load provided parameters
        if P_peak is not None:
            self.model.value("system_capacity", P_peak / 1000)
        if v is not None:
            self.model.value("azimuth", 180 + v)
        if inclination is not None:
            self.model.value("tilt", inclination)
        if PR is not None:
            self.model.value("losses", 100 - PR * 100)

        self.model.execute()

        # Here, we initialise the model based on the average daily energy that
        # you would like to generate, based on a certain year's data.

        # FIXME TODO Allow option of specifyig fixed PV size, and calculating the
        # system capacity from that.

        # nominal_power [kW] = land_area [m^2] / 5.263 [m^2/kW] * GCR

        if E_desired is not None:

            # We are going to try and figure out the required system capacity,
            # based on the weather data.
            if P_peak is not None:
                raise ValueError("P_peak must not be specified when " +
                                 "E_desired is specified.")

            average_daily_energy = self.model.Outputs.annual_energy / 365

            if abs(average_daily_energy - E_desired) != 0:
                # Calculating the energy generated from a panel of 1 kW nominal
                # power. This value is a linear constant.
                daily_kWh_per_kw = self.model.Outputs.kwh_per_kw / 365

                # Dividing the desired energy generation by the linear constant
                # to get the required nominal power.
                system_capacity = E_desired / daily_kWh_per_kw
                self.model.value("system_capacity", system_capacity)

                self.model.execute()
        self.system_capacity = self.model.SystemDesign.system_capacity

    def get_results(self):
        return self.model.Outputs.ac  # AC output in watts

    def get_size(self, GCR: float = 1):
        # GCR: Ground Coverage Ratio. In a roof installation, this is 1.
        # However, in an open rack system, this won't be 1, as there will be
        # self-shading from the sub-arrays.

        # SAM approximates the land area as:
        # land_area [m^2] = nominal_power [kW] * 5.263 [m^2/kW] / GCR

        self.land_area = self.system_capacity * 5.263 / GCR
        return self.land_area

    def calc_costs(self):

        # TODO Hard-coding this for now. Convert this to SAM's LCOE model.
        # (More configurable.) E.g. We need to allow the FCR to change based on
        # the number of years that we consider to be the project's lifetime.

        tcc_per_kW = 975  # Total capital cost: EUR/kW
        foc_per_kW = 12.2  # Fixed operating costs: EUR/kW. (Maintenance, etc.)
        tcc = tcc_per_kW * self.system_capacity  # Total capital cost: EUR
        foc = foc_per_kW * self.system_capacity  # Fixed operating costs: EUR
        fcr = 0.08160257  # Fixed charge rate. (Percentage of initial
                          # investment that needs to be paid annually.) This
                          # value is for a 20 year loan period.
        aep = self.model.Outputs.annual_energy  # Annual energy production: kWh
        voc = 0  # Variable operating costs [EUR/kw]
        self.lcoe = (fcr * tcc + foc) / aep + voc

class System:

    class FCVs:

        def __init__(self, distance: pd.DataFrame):

            self.distance = distance
            self.vehicle_ids = distance.columns

            self.fcv_list: t.List[FuelcellVehicle] = \
                [FuelcellVehicle(distance[vehicle_id], vehicle_id) for
                 vehicle_id in distance.columns]

        def calc_energies(self):

            self.mean_energies = []
            self._energies = []
            for fcv in self.fcv_list:
                self._energies.append(fcv.calc_energies())
                self.mean_energies.append(fcv.mean_energy)

            self.energies = pd.concat(self._energies, keys=self.vehicle_ids,
                                      names=['vehicle_id', 'date'])

            self.energy = self.energies[['energy']].unstack(0)

            return self.energy

        def calc_capex(self,):

            self.capex = 0
            for fcv in self.fcv_list:
                self.capex += fcv.calc_capex()

            return self.capex


    def __init__(self, distance: pd.DataFrame):
        """ The system class, contains objects for the fleet of FCVs, and the
            Hydrogen production plants. The fleet consists of FCV objects with
            tank objects. The filling stations consist of tank, compressor and
            electrolyser objects. """

        # Create a population of FCVs, corresponding to the input files of the
        # scenario.

        self.fcvs = self.FCVs(distance)

        self.fcvs.calc_energies()

        self.compressor = Compressor(self.fcvs.energies['energy'].unstack(0))
        self.electrolyser = Electrolyser(self.fcvs.energies['energy'].unstack(0))

        self.compressor.calc_energy()
        self.electrolyser.calc_energy()
        self.energy: pd.Series = (self.electrolyser.energy +
                                  self.compressor.energy_cost)
        self.pv = Photovoltaic(E_desired=self.energy.sum(1).mean())

    def calc_costs(self):
        """Calculate the capital and operating expenses of the system"""
        self.capex = self.fcvs.calc_capex() + \
                     self.compressor.calc_capex() + \
                     self.electrolyser.calc_capex()

        # TODO Operating expenses...?

    def print(self):
        print(f"{self.fcvs.energy.sum(1).mean():.2f} kWh of " +
              f"hydrogen used by the {len(self.fcvs.vehicle_ids)} FCV(s) to " +
              f"travel an average of {self.fcvs.distance.mean(1).mean()} km.\n")
        print(f"{self.energy.sum(1).mean():.2f} kWh electricity is required " +
              "to produce this hydrogen.\n")
