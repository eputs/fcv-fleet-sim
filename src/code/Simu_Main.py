#!/usr/bin/env python

from matplotlib import pyplot as plt
from matplotlib import gridspec as gridspec
import numpy as np
import pandas as pd

import os
import datetime
from statistics import mean

from Simu_Klassen import *
from Simu_Regelstrategie import simulateObjects
from Simu_Reserveprofil import reserveProfile
from parameter_base_case import *

from pathlib import Path


def Iteration():

    # Festlegung der Batterie und Elektrolyseur Konfigurationen
    electrolyserSizes = np.arange(start=0, stop=500, step=200)
    batterySizes = np.arange(start=5, stop=15, step=5)

    l = len(electrolyserSizes) * len(batterySizes)
    i = 0
    printProgressBar(
        0,
        l,
        prefix="Progress:",
        suffix="Complete",
        length=50,
    )
    # Iteriere über alle Batterie und Elektrolyseur Größen
    for elecSize in electrolyserSizes:
        for batSize in batterySizes:

            # TODO Replace this with a SAM battery.

            # Initialisiere Batterie
            battery1 = Battery(
                maxPower=(batSize / 2) * 1000,
                maxEnergy=batSize,
                energy=0.0,
                efficiency=eff_bat,
                systemCost=K_system_batterie,
                CO2_KgperkWh=Batterie_CO2,
            )

            # Initialisiere Wasserstoffspeicher
            h2 = H2(maxEnergy=100000, hydrogenPrice=wasserstoffverguetung)

            # Initialisiere Netz
            grid1 = Grid(
                electricityPrice=c_netz,
                feedInTariff=c_einspeisung,
                CO2perkWh=Strommix_CO2,
            )

            # Initialisiere Elektrolyseur (Unterklasse von Wasserstoffspeicher)
            e1 = h2.createElectrolyser(
                maxPower=elecSize,
                efficiency=eff_elektrolyse,
                systemCost=K_system_wasserstoff,
                CO2_KgperkWh=H2System_CO2,
            )

            # Erstelle Batteriereserve-Array
            batteryReserveArray, summerReserve, winterReserve = createRules(
                battery1.maxEnergy, 2, 7
            )

            watertank1 = Watertank(volume=200, efficiency=eff_elektrolyse_waerme)

            dictParam = simulateObjects(
                battery=battery1,
                electrolyser=e1,
                fuellcell=None,
                hydrogentank=h2,
                grid=grid1,
                watertank=watertank1,
                Lastprofilnummer=31,
                batteryReserveArray=batteryReserveArray,
                summerReserve=summerReserve,
                winterReserve=winterReserve,
                visualize=False,
            )

            i += 1
            printProgressBar(
                i,
                l,
                prefix="Progress:",
                suffix="Complete",
                length=50,
            )

    # Exportiere und visualisiere Ergebnisse aus dict
    ExcelExport(dictParam)
    plotBarChart(dictParam)
    plotLineChart(dictParam)


def IterateVariants():
    # In dieser Funktion werden verschiedene Varianten simuliert und Simulationsein- und Ausgänge nach Excel exportiert
    # Da es viele variierte Systemparameter gibt ist eine Visualisierung nicht möglich und die Ergebnisse werden nach Excel exportiert

    # Arrays durch die in geschachtelten Schleifen iteriert wird
    # Dauer je nach Variante ca. 7 Sekunden

    wReserve = [8]  # Batteriereserve/n für den Winter in kWh
    sReserve = [4]  # Batteriereserve/n für den Sommer in kWh
    elektrolyserPower = [1000]  # Elektrolyseurleistung/en in W
    batteryCapacity = [10]  # Batteriekapazität/en in kWh

    _ = input("Would you like to save plots as interactive files? (Warning: Large file size!) y/[n]: ")
    visualize_pickle = False if _.lower() != 'y' else True

    # Geschachtelte Schleifen um Varianten zu erzeugen
    for w, wRes in enumerate(wReserve):
        for s, sRes in enumerate(sReserve):
            for e, ePower in enumerate(elektrolyserPower):
                for b, bCapacity in enumerate(batteryCapacity):

                    # Initiate objects
                    battery1 = Battery(
                        maxPower=(bCapacity / 2) * 1000,
                        maxEnergy=bCapacity,
                        energy=0.0,
                        efficiency=eff_bat,
                        systemCost=K_system_batterie,
                        CO2_KgperkWh=Batterie_CO2,
                    )

                    h2 = H2(
                        maxEnergy=100000, hydrogenPrice=wasserstoffverguetung
                    )  # gesamter Wasserstoff wird verkauft

                    grid1 = Grid(
                        electricityPrice=c_netz,
                        feedInTariff=c_einspeisung,
                        CO2perkWh=Strommix_CO2,
                    )

                    watertank1 = Watertank(
                        volume=200, efficiency=eff_elektrolyse_waerme
                    )

                    e1 = h2.createElectrolyser(
                        maxPower=ePower,
                        efficiency=eff_elektrolyse,
                        systemCost=K_system_wasserstoff,
                        CO2_KgperkWh=H2System_CO2,
                    )

                    batteryReserveArray, summerReserve, winterReserve = createRules(
                        battery1.maxEnergy, sRes, wRes
                    )

                    # Simulation durchführen
                    dictParam = simulateObjects(
                        battery=battery1,
                        electrolyser=e1,
                        fuellcell=None,
                        hydrogentank=h2,
                        grid=grid1,
                        watertank=watertank1,
                        Lastprofilnummer=31,
                        batteryReserveArray=batteryReserveArray,
                        summerReserve=summerReserve,
                        winterReserve=winterReserve,
                        visualize=True,
                        visualize_pickle=visualize_pickle,
                    )

    ExcelExport(dictParam)


def createRules(batteryMaxEnergy, summerReserve, winterReserve):

    # TODO: Make this function support Southern hemisphere scenarios.

    if batteryMaxEnergy > 0:
        summerRes = summerReserve / batteryMaxEnergy * 100
        winterRes = winterReserve / batteryMaxEnergy * 100
    else:
        summerRes = 0
        winterRes = 0

    # Wert der Batteriereserve in Abhängigkeit der Zeit festlegen
    rules = (
        {
            "Monate": [datetime.datetime(2020, 1, 1), datetime.datetime(2020, 3, 1)],
            "Tageszeit": [datetime.time(17), datetime.time(9)],
            "Wert": winterRes,
        },
        {
            "Monate": [datetime.datetime(2020, 1, 1), datetime.datetime(2020, 3, 1)],
            "Tageszeit": [datetime.time(9), datetime.time(17)],
            "Wert": winterRes,
        },
        {
            "Monate": [datetime.datetime(2020, 3, 1), datetime.datetime(2020, 10, 1)],
            "Tageszeit": [datetime.time(17), datetime.time(9)],
            "Wert": summerRes,
        },
        {
            "Monate": [datetime.datetime(2020, 3, 1), datetime.datetime(2020, 10, 1)],
            "Tageszeit": [datetime.time(9), datetime.time(17)],
            "Wert": summerRes,
        },
        {
            "Monate": [datetime.datetime(2020, 10, 1), datetime.datetime(2020, 12, 31)],
            "Tageszeit": [datetime.time(17), datetime.time(9)],
            "Wert": winterRes,
        },
        {
            "Monate": [datetime.datetime(2020, 10, 1), datetime.datetime(2020, 12, 31)],
            "Tageszeit": [datetime.time(9), datetime.time(17)],
            "Wert": winterRes,
        },
    )

    # Array mit Batteriereserve erzeugen
    return reserveProfile(rules), summerRes, winterRes


def plotBarChart(dictParam) -> plt.Figure:

    x = range(len(dictParam["Batterie Ladeleistung [W]"]))
    x = np.arange(len(dictParam["Batterie Ladeleistung [W]"]))
    labels = []
    for i in range(len(dictParam["Batterie Ladeleistung [W]"])):
        string = f'{dictParam["Batterie Kapazität [kWh]"][i]}kWh-{dictParam["Elektrolyseur Leistung [W]"][i]}W'
        labels.append(string)
    labels = [label.replace("-", "\n") for label in labels]

    fig = plt.figure()
    ax = plt.subplot(111)

    width = 0.3

    # Kosten Batterie
    data = dictParam["Kosten Batterie [€]"]
    ax.bar(x, data, width=width, color="orangered", label="Batterie")

    # Kosten Wasserstoff
    data = dictParam["Kosten Wasserstoffsystem [€]"]
    bottom = dictParam["Kosten Batterie [€]"]
    ax.bar(x, data, bottom=bottom, width=width, color="b", label="Wasserstoffsystem")

    # Kosten Strombezug
    data = dictParam["Kosten Strombezug [€]"]
    x1 = dictParam["Kosten Wasserstoffsystem [€]"]
    bottom = [a + b for a, b in zip(x1, bottom)]
    ax.bar(x, data, bottom=bottom, width=width, color="g", label="Strombezug")

    # Gesamtkosten
    data = dictParam["Gesamtkosten mit Speichern [€]"]
    ax.bar(x + width, data, width=width, color="dodgerblue", label="Gesamtkosten")

    # Erlös Wasserstoff
    data = [-x for x in list(dictParam["Erlös Wasserstoffverkauf [€]"])]
    ax.bar(x, data, width=width, color="lime", label="Wasserstoffverkauf")

    ax.set(xticks=x + width / 2, xticklabels=labels)
    ax.set_ylabel("Kosten/€")
    ax.set_title("Kostenaufteilung")
    ax.legend()
    ax.set_axisbelow(True)
    ax.grid(color="b")
    plt.show()

    path = Path(os.path.abspath(__file__)).parents[1].joinpath('export',
        f'barplot_{datetime.datetime.now().strftime("%Y_%m_%d_%H_%M_%S")}.svg')

    plt.savefig(path)


def plotLineChart(dictParam):

    ax1 = plt.subplot(1, 2, 1)
    ax1.set_title("Ökonomie")
    ax1.set_ylabel("Kosten/€")
    ax1.set_xlabel("Batteriekapazität/kWh")
    ax1.set_xlim(xmin=5, xmax=20)

    ax2 = plt.subplot(1, 2, 2, sharex=ax1)
    ax2.set_title("Ökologie")
    ax2.set_ylabel("CO2 pro kWh")
    ax2.set_xlabel("Batteriekapazität/kWh")

    electrolyserPower = dictParam["Elektrolyseur Leistung [W]"]

    li = list(dict.fromkeys(electrolyserPower))

    for i, val in enumerate(li):
        indices = [
            i for i in range(len(electrolyserPower)) if electrolyserPower[i] == val
        ]

        x = [dictParam["Batterie Kapazität [kWh]"][j] for j in indices]
        y1 = [dictParam["Gesamtkosten mit Speichern [€]"][j] for j in indices]
        y2 = [dictParam["CO2 Kennzahl [kgCO2/kWh]"][j] for j in indices]
        # print(indices, x, y1, y2)

        ax1.plot(x, y1, label=f"{val}W", linewidth=2)
        ax2.plot(x, y2, label=f"{val}W", linewidth=2)
        ax1.legend()
        ax2.legend()
    ax1.axhline(
        dictParam["Gesamtkosten ohne Speicher [€]"][0],
        color="red",
        linestyle="--",
        linewidth=2,
        label="Ohnefall",
    )
    ax2.axhline(
        dictParam["CO2 Kennzahl ohne Speicher [kgCO2/kWh]"][0],
        color="red",
        linestyle="--",
        linewidth=2,
        label="Ohnefall",
    )
    ax1.legend()
    ax2.legend()
    plt.show()

    path = Path(os.path.abspath(__file__)).parents[1].joinpath('export',
        f'lineplot_{datetime.datetime.now().strftime("%Y_%m_%d_%H_%M_%S")}.svg')

    plt.savefig(path)

def ExcelExport(dictParam):
    # Simulationsparameter nach Excel exportieren

    path = os.path.abspath(
        os.path.join(
            os.path.dirname(__file__),
            "..",
            'export',
            f'Series_{datetime.datetime.now().strftime("%Y_%m_%d_%H_%M_%S")}.xlsx',
        )
    )

    df = pd.DataFrame.from_dict(dictParam)
    df.to_excel(path)


def printProgressBar(
    iteration,
    total,
    prefix="",
    suffix="",
    decimals=1,
    length=100,
    fill="█",
    printEnd="\r",
):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + "-" * (length - filledLength)
    print(f"\r{prefix} |{bar}| {percent}% {suffix}", end=printEnd)
    # Print New Line on Complete
    if iteration == total:
        print()


if __name__ == "__main__":
    # Iteration()
    IterateVariants()
