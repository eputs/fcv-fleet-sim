"""Module to make scenario sub-directories if they don't exist."""

from pathlib import Path
import shutil
from typing import Dict
import json
import platform
import fcv_fleet_sim as fs


def initialise_scenario(scenario_dir: Path, **kwargs):
    """Initialise the scenario's folder structure."""

    def create_dir(scenario_dir: Path, sub_dirs: Dict):
            """Make scenario sub-directories if they don't exist."""
            # Make the scenario_dir.
            scenario_dir.mkdir(parents=True, exist_ok=True)
            # If there are no sub_dirs, quit the function.
            if sub_dirs is None:
                return
            # If there are sub_dirs, create each of them with *their* sub_dirs.
            for sub_dir, sub_sub_dirs in sub_dirs.items():
                sub_dir = scenario_dir.joinpath(sub_dir)
                create_dir(sub_dir, sub_sub_dirs)

    def copy_default_files():

        src_init_dir = Path(__file__).parent.joinpath('Initialisation_Files')

        # Copy configuration files:

        scenario_init_dir = scenario_dir.joinpath('0_Scenario_Initialisation')

        for file in src_init_dir.joinpath('configs').glob('*'):
            if file.name == "__init__.py":
                continue
            from_file = src_init_dir.joinpath('configs', file.name)
            to_file = scenario_init_dir.joinpath('Inputs', 'Configs', file.name)
            shutil.copy(from_file, to_file)

        for file in src_init_dir.joinpath('instructions').glob('*'):
            if file.name == "__init__.py":
                continue
            from_file = src_init_dir.joinpath('instructions', file.name)
            to_file = scenario_init_dir.joinpath('Instructions', file.name)
            shutil.copy(from_file, to_file)

    def create_readme():
        with open(str(scenario_dir.joinpath('0_Scenario_Initialisation',
                'Instructions', 'directory_structure.json')), 'w') as f:
            json.dump(fs.SCENARIO_DIR_STRUCTURE, f, indent=4)
        return

    create_dir(scenario_dir, fs.SCENARIO_DIR_STRUCTURE)
    copy_default_files()
    create_readme()
    print("Follow the initialisation instructions in: \n\t" +
          str(scenario_dir.joinpath("initialisation-instructions.md")) +
          "\n\tbefore preoceeding with the remaining simulation steps.")
