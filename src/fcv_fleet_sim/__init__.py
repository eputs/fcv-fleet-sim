from .scenario_initialisation import scenario_initialisation
from .data_preprocessing import data_filtering, distance_calculation
from .data_visualisation import animation, mapping
from .system_simulation import system_simulation
from .optimisation import optimisation

SCENARIO_DIR_STRUCTURE = {
    '0_Scenario_Initialisation': {
        'Inputs': {
            'Traces': None,
            'Configs': None,
            'Weather': None,
            # 'Map': None,
        },
        'Instructions': None,
    },
    '1_Data_Preprocessing': {
        'Filtered_Data': None,
        'Distances': None,
    },
    '2_Data_Visualisations': {
        'Heatmaps': None,
        'Route_Animations': None,
    },
    '3_System_Simulation': {
        'Outputs': None,
        'Logs': None,
        'Results': None,
    },
}

MODULES = """
0. scenario_initialisation
1. data_preprocessing
    1.1. data_filtering
    1.2. distance_calculation
2. data_visualisation
    2.1. mapping
    2.2. animation
3. system_simulation
4. optimisation
"""

from pathlib import Path
from typing import Iterable, SupportsFloat
import argparse
import time


def run(scenario_dir: Path, steps: Iterable[SupportsFloat],
        autorun: bool = False, **kwargs):
    """Run specified steps of data_analysis."""

    if 0 in steps:
        print("### Scenario Initialisation ###")
        scenario_initialisation.initialise_scenario(scenario_dir, **kwargs)
    if 1 in steps or 1.1 in steps:
        print("### Data Filtering ###")
        data_filtering.filter_scenario(scenario_dir, **kwargs)
    if 1 in steps or 1.2 in steps:
        print("### Distance Calculation ###")
        distance_calculation.calc_distances(scenario_dir, **kwargs)
    if 2 in steps or 2.1 in steps:
        print("### Mapping ###")
        mapping.map_scenario(scenario_dir, **kwargs)
    if 2 in steps or 2.2 in steps:
        print("### Animation ###")
        animation.animate_scenario(scenario_dir, **kwargs)
    if 3 in steps:
        print("### Simulation ###")
        system_simulation.simulate_system(scenario_dir, **kwargs)
    if 4 in steps:
        print("### Optimisation ###")
        optimisation.optimise_system(scenario_dir, **kwargs)


def main():

    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument(
        'scenario_dir', nargs='?', default=None, metavar='str',
        help='The root directory of the scenario to be simulated.')
    parser.add_argument(
        '--steps', default=None, metavar='List[float]',
        help="The steps to be run on the scenario as a comma-seperated list " +
             " of floats without spaces (e.g. '0,1.1,2').")
    parser.add_argument(
        '--autorun', action='store_true')
    parser.add_argument(
        '--debug', action='store_true')
    args = parser.parse_args()

    if args.debug:
        import pdb
        pdb.set_trace()

    if args.scenario_dir:
        scenario_dir = Path(args.scenario_dir)
    else:
        _ = input("Specify scenario root directory: ")
        scenario_dir = Path(_)

    while scenario_dir.exists() is False:
        print("The path does not exist!")
        _ = input("Specify scenario root directory: ")
        scenario_dir = Path(_)

    while True:
        if args.steps:
            steps_str = args.steps
        else:
            print("Available steps: ")
            print(MODULES)
            steps_str = input("Specify steps to be run as a comma-seperated " +
                              "list of floats without spaces (e.g. '0,1.1,2'): ")
        try:
            steps = [float(step) for step in steps_str.split(',')]
        except ValueError:
            print("Error: That is not a valid selection! Try again...")
            time.sleep(1)
            continue
        break

    # TODO: Implement autorun properly!!!
    kwargs = {'autorun': args.autorun}

    # Run all the steps
    run(scenario_dir, steps=steps, **kwargs)
