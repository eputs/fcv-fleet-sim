#!/usr/bin/env python3
"""This program runs all the components of the data_processing_ev package."""

import fcv_fleet_sim as fs

if __name__ == "__main__":
    fs.main()
