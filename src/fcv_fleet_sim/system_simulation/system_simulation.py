import fcv_fleet_sim.system_simulation.simulation_models as models
from pathlib import Path
import pandas as pd
from matplotlib import pyplot as plt
import pickle


def simulate_system(scenario_dir: Path, **kwargs):

    distance_files = sorted([*scenario_dir.joinpath(
        '1_Data_Preprocessing', 'Distances').glob('*.csv')])
    vehicle_ids = [distance_file.stem for distance_file in distance_files]

    daily_fcv_energies = []
    daily_system_energies = []

    distances_list = []

    for distance_file in distance_files:

        distances = pd.read_csv(distance_file)
        distances['Date'] = pd.to_datetime(distances['Date'])
        distances = distances.set_index('Date')
        distances = distances['Distance (m)']
        distances.name = 'distance'
        distances /= 1000
        distances_list.append(distances)

    system = models.System(scenario_dir, pd.concat(distances_list, axis=1, keys=vehicle_ids))

    # TODO Delete or use:
    # fcv_energy: pd.DataFrame = system.fcvs.energies['energy'].unstack(0)

    system.pv.calc_costs()

    system.calc_costs()

    ###########
    # Outputs #
    ###########

    stats = system.print()

    outputs_dir = scenario_dir.joinpath('3_System_Simulation', 'Results')
    with open(outputs_dir.joinpath('stats.txt'), 'w') as f:
        f.writelines(stats)


    # Plotting results


    # Save plots

    figs_titles = system.plot()

    outputs_dir = scenario_dir.joinpath('3_System_Simulation', 'Outputs')

    for fig, title in figs_titles:
        fig.savefig(outputs_dir.joinpath(f'{title}.svg'))
        with open(outputs_dir.joinpath(f'{title}.fig.pickle'),
                  'wb') as f:
            pickle.dump(fig, f)

    plt.show()
