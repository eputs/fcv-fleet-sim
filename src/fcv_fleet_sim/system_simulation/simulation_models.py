from math import sqrt
import numpy as np
import pandas as pd
import PySAM.BatteryStateful as bs
import PySAM.Pvwattsv8 as pv
from pathlib import Path
import json
import typing as t
from matplotlib import pyplot as plt


BATT_CONFIG_PATH = None
PVWATTS_CONFIG_PATH = None
SCENARIO_DIR = None

_ENERGY_IN_H2 = 33  # kWh/kg  # TODO Find citation.


class Accountant:

    def present_value(A: float, i: float, f: int, t: int):
        """Calculates the present value of a set of periodic installments.

        A: instalment amount.
        i: interest rate per year.
        f: frequency of instalments per year.
        t: total number of years that the installments are to be payed.
        """
        return A * (1 - (1 + i/f) ** (-t*f)) / (i/f)


class Battery:

    # TODO Cost calculation.

    def __init__(
            self, maxPower=None, maxEnergy=None,
            energy=None,  # Note: Energy is deprecated.
            efficiency=None,  # Note: efficiency is deprecated.
            systemCost: float = 50,  # From parameter_base_case
            CO2_KgperkWh: float = 150,  # kg/kWh nutzbare Speicherkapazität
            ):

        # Loading the model parameters from the configuration file.
        self.model = bs.new()
        with open(BATT_CONFIG_PATH, 'r') as config_file:
            data = json.load(config_file)
            for key, val in data.items():
                if key != 'number_inputs' and key != '__':
                    self.model.value(key, val)

        if maxPower is not None:
            self.maxPower = maxPower  # In Watts
        else:
            self.maxPower = min(self.model.StatePack.P_chargeable * 1000,
                                self.model.StatePack.P_dischargeable * 1000)

        # if maxEnergy is not None:
        #     self.model.value('nominal_energy', maxEnergy)
        # if energy is not None:
            # self.model.value('initial_SOC',
            #                  energy / self.model.value('nominal_energy') * 100)
        # if efficiency is not None:
        #     log.error('Efficiency is deprecated. Use more detailed battery ' +
        #               'modelling parameters.')
        #     input('Press enter to ignore the efficiency parameter and ' +
        #           'continue... Else, press <Ctrl> + C to quit the program.')

        self.model.value('dt_hr', 1 / 60)  # Run the simulation for a one-minute timestep.
        self.model.setup()

        self.maxEnergy = self.model.ParamsPack.nominal_energy
        self.voltage = self.model.ParamsPack.nominal_voltage
        self.systemCost = systemCost
        self.CO2_KgperkWh = CO2_KgperkWh

        self.SOC = self.model.StatePack.SOC
        self.energy = self.SOC * self.maxEnergy
                      # The current energy level of the battery.
        self.power = self.model.StatePack.P * 1000
        self.energyIn = 0
        self.energyOut = 0
        self.powerLeftover = 0 * 1000

    def powerIn(self, P, reserve=None):

        # TODO Reserve parameter is deprecated.

        # In elysim, if P<0, it means that the system is trying to draw power
        # from  the battery. If P>0, it means that the system is trying to
        # charge the battery with excess (solar) power that is available. This is
        # oppisite to the SAM model's convention. Therefore, we make P = -P. We
        # will also invert the output values of the model. Also, ElySim's power
        # is in W, while SAM expects it in kW.

        # Limit the input power based on the maxPower parameter.
        P_in = P  # Make a backup of the original input power, before capping
                  # it at maxPower.
        if abs(P) > self.maxPower:
            P = self.maxPower

        P = -P/1000  # To kW.

        # Save a copy of the energy of the battery before the power is fed in.
        self.prev_energy = self.model.StatePack.V * self.model.StatePack.Q / 1000

        # Execute the timestep!
        self.model.value('input_power', P)
        self.model.execute(1)

        # Revert power to ElySimulation's understanding.
        P = -P * 1000

        # Extract the results important to us.
        self.energy = self.model.StatePack.V * self.model.StatePack.Q / 1000
                      # The current energy level of the battery in kWh.
        # self.SOC = self.energy / self.maxEnergy * 100
        self.SOC = self.model.StatePack.SOC
                   # The current energy level of the battery as a percentage.
        self.power = -self.model.StatePack.P * 1000
                     # The power charged into the battery, expressed in W.

        if self.prev_energy >= self.energy:
            self.energyOut = abs(self.prev_energy - self.energy)
        else:
            self.energyIn = abs(self.prev_energy - self.energy)

        self.powerLeftover = P_in - self.power


class Battery_Old:

    def __init__(
        self, maxPower, maxEnergy, energy, efficiency, systemCost, CO2_KgperkWh
    ):
        self.maxPower = maxPower
        self.maxEnergy = maxEnergy
        self.energy = energy
        self.efficiency = efficiency
        self.systemCost = systemCost
        self.CO2_KgperkWh = CO2_KgperkWh
        self.SOC = 0
        self.power = 0
        self.energyIn = 0
        self.energyOut = 0
        self.powerLeftover = 0

    def powerIn(self, P, reserve):

        if abs(P) <= self.maxPower:
            # when smaller than maximum charge rate
            self.power = P
        else:
            # else charge at maximum battery rate
            if P < 0:
                self.power = -self.maxPower
            else:
                self.power = self.maxPower

        # integrate power over time --> energy
        if P >= 0:
            # multiply efficiency when charging
            newEnergie = self.energy + (self.power * sqrt(self.efficiency)) / 60 / 1000
        else:
            # divide efficiency when discharging
            newEnergie = self.energy + (self.power / sqrt(self.efficiency)) / 60 / 1000

        # dont charge or discharge when full or empty
        if (newEnergie <= self.maxEnergy and newEnergie >= reserve) or (
            newEnergie < reserve and newEnergie > 0
        ):

            if newEnergie > self.energy:
                # self.energyIn = self.energyIn + newEnergie - self.energy
                self.energyIn = self.energyIn + (newEnergie - self.energy) / sqrt(
                    self.efficiency
                )
            else:
                # self.energyOut = self.energyOut + self.energy - newEnergie
                self.energyOut = self.energyOut + (self.energy - newEnergie) * sqrt(
                    self.efficiency
                )
            self.energy = newEnergie

        else:
            self.power = 0
            # self.energy = E_bat_old
        try:
            self.SOC = (self.energy / self.maxEnergy) * 100
        except:
            self.SOC = 0
        self.powerLeftover = P - self.power


class FuelcellVehicle:

    def __init__(self, distances:pd.Series, vehicle_id,
                 config_file: Path = None):  # , model_parameters: dict):
        """
        Initialise the model parameters{TODO::, using a SUMO typ.xml file}.

        The distances dataframe must have two columns. The first column with
        the various dates, and the second column with the distances travelled
        on those dates.
        """

        # Loading class model parameters
        if config_file is None:
            config_file = SCENARIO_DIR.joinpath('0_Scenario_Initialisation',
                'Inputs', 'Configs', 'fcv.json')

        with open(config_file, 'r') as f:
            self.config = json.load(f)

        # Take this from model_parameters instead.
        self.vehicle_id = vehicle_id
        self.tank = _Tank()
        self.distances = distances.loc[
            distances.first_valid_index():distances.last_valid_index()]
        self.energies: pd.DataFrame = None

    def calc_energies(self) -> pd.DataFrame:
        """ Calculate energy required to move the vehicle x kms per day. """

        self.energies = pd.DataFrame(self.distances)
        self.energies.columns = ['distance']
        self.energies['electrical_energy'] = self.config['energy_efficiency'] * self.energies['distance']
        self.energies['usable_energy'] = self.energies['electrical_energy'] / self.config['fuelcell_efficiency']
        self.energies['energy'] = self.energies['usable_energy'].map(self.tank.calc_energy)

        self.mean_energy = self.energies['energy'].mean()

        return self.energies

    def calc_capex(self) -> float:

        return self.config['tcc']
                         # FIXME In a 20 year lifecycle, you'll need to repeat
                         # the cost of the FCVs. You can also account for the
                         # resale values of the last taxis who are not used for
                         # that many years.

    def calc_opex(self, ) -> float:  # fuel_rate

        # From Pistoia et al (2010) Electric and Hybrid Vehicles, Chapter 2,
        # Section 4

        # TODO config
        _ice_mbt_tcc = 30000  # €
        insurance_rate = 0.025 + 0.025 * (self.config['tcc'] / _ice_mbt_tcc)  # €/km

        data_period = (self.distances.index[-1] -
                       self.distances.index[0]).days / 365

        maintenance_cost_p_year = sum([distance * self.config['maintenance_rate']
                                       for distance in self.distances[self.distances.notna()]])/data_period
        insurance_cost_p_year = sum([distance * insurance_rate for distance in self.distances[self.distances.notna()]])/data_period
        # fuel_cost_p_year = sum([energy * fuel_rate for energy in self.energies['energy']])/data_period

        self.opex = Accountant.present_value(
            A=maintenance_cost_p_year + insurance_cost_p_year,  # + fuel_cost_p_year
            i=0.08,  # TODO Make this a system parameter! Favourable interest:
                     # 8 %, unfavourable interest: 20 %.
            f=1,
            t=self.config['lifetime'],
        )
        return self.opex


class _Tank:
    """Keeps track of the energy in the vehicle."""

    def __init__(self):

        # Get this from model parameter file.
        self.nom_pressure = 700  # Bar
        self.energy_efficiency = 0.95  # 95%

    def calc_energy(self, usable_energy) -> float:

        """ Calculate the energy loss in the tank (due to daily pressure
        leaks?) """

        self.usable_energy = usable_energy
        self.energy = usable_energy / self.energy_efficiency
            # Energy stored in tank.
        return self.energy


class Compressor:
    """Pressurizes the tank, and reports how much energy was lost in the
       process."""

    def __init__(self, daily_hydrogen_energy: pd.DataFrame):

        self.compression_energy = 0.10  # 10% of the Hydrogen energy is
                                        # required for compression.
        self.tcc_per_kW = 8000  # EUR/kW This is quite high! Can it be true??
        self.daily_hydrogen_energy = daily_hydrogen_energy

    def calc_energy(self) -> float:

        """Calculate the energy loss due to pressurizing the tank at
           its nominal pressure."""  # TODO

        self.energy_cost = self.daily_hydrogen_energy * self.compression_energy

        return self.energy_cost

    def calc_capex(self) -> float:
        self.avg_power = self.energy_cost.sum(1).mean() / 24
            # Assuming constant operation.
        self.capex = self.avg_power * self.tcc_per_kW
        return self.capex

    def calc_opex(self,) -> float:
        maintenance_factor = 0.05  # Annual maintenance is 5% of the tcc.
        maintenance_cost = maintenance_factor * self.tcc_per_kW
        electricity_rate = 0.30  # TODO Make electricity_rate a parameter of System.
        electricity_cost = self.avg_power * 365 * electricity_rate

        self.opex = Accountant.present_value(
            A=maintenance_cost + electricity_cost,
            i=0.08,
            f=1,
            t=20,
        )
        return self.opex


class Electrolyser:
    def __init__(self, daily_hydrogen_energy: pd.DataFrame,
                 config_file: Path = None):
        self.daily_hydrogen_energy = daily_hydrogen_energy

        if config_file is None:
            config_file = SCENARIO_DIR.joinpath('0_Scenario_Initialisation',
                'Inputs', 'Configs', 'electrolyser.json')

        with open(config_file, 'r') as f:
            self.config = json.load(f)

    def calc_energy(self) -> float:
        self.energy = self.daily_hydrogen_energy / self.config['efficiency']
        return self.energy

    def calc_capex(self) -> float:
        # first we need to calculate the size of the electrolyser.
        self.avg_power = self.energy.sum(1).mean() / 24
            # Assuming that the electrolyser is on the whole day.
            # energy[kWh] / time[h] = power[kW]
        self.capex = self.avg_power * self.config['tcc_per_kW']
        return self.capex

    def calc_opex(self, system_config: dict) -> float:
        maintenance_cost = self.config['maintenance_factor'] * \
                           self.config['tcc_per_kW']
        electricity_cost = self.avg_power * 365 * system_config['electricity_rate']

        self.opex = Accountant.present_value(
            A=maintenance_cost + electricity_cost,
            i=0.08,
            f=1,
            t=20,
        )

        return self.opex

    def calc_co2(self) -> float:

        self.co2 = self.power * self.config['co2_rate']
        return self.co2


# TODO Delete this class? Not useful for macrosimulation...
class H2:
    def __init__(self, maxEnergy, hydrogenPrice):
        self.maxEnergy = maxEnergy  # Maximum energy that can be stored in the
                                    # tank.
        self.hydrogenPrice = hydrogenPrice
        self.energy = 0
        self.hydrogentankLevel = 0

    def createElectrolyser(self, maxPower, efficiency, systemCost, CO2_KgperkWh):
        return H2.Electrolyser(self, maxPower, efficiency, systemCost, CO2_KgperkWh)

    def createFuelcell(self, maxPower, efficiency):
        return H2.Fuelcell(self, maxPower, efficiency)

    class Electrolyser:
        def __init__(self, outer, maxPower, efficiency, systemCost, CO2_KgperkWh):
            self.outer = outer
            self.maxPower = maxPower
            self.efficiency = efficiency
            self.systemCost = systemCost
            self.CO2_KgperkWh = CO2_KgperkWh
            self.power = 0
            self.energyIn = 0
            self.minutesTurnedOn = 0

        def powerIn(self, P):
            """ Calculates the ammount of energy of Hydrogen generated, if the
                electrolyser is run at power, P, for 1 minute. """

            if P >= 0:
                if P > 0:
                    self.minutesTurnedOn += 1
                if P <= self.maxPower:
                    self.power = P
                else:
                    self.power = self.maxPower

                newEnergy = (
                    self.outer.energy + (self.power * self.efficiency) / 60 / 1000
                )

                if newEnergy <= self.outer.maxEnergy and newEnergy >= 0:
                    self.outer.energy = newEnergy
                else:
                    self.power = 0
                self.energyIn = self.energyIn + self.power / 60 / 1000
                self.outer.hydrogentankLevel = (
                    self.outer.energy / self.outer.maxEnergy
                ) * 100

    class Fuelcell:
        def __init__(self, outer, maxPower, efficiency):
            self.outer = outer
            self.maxPower = maxPower
            self.efficiency = efficiency
            self.power = 0

        def powerIn(self, P):
            if P <= 0:
                if -P <= self.maxPower:
                    self.power = P
                else:
                    self.power = self.maxPower

                newEnergy = (
                    self.outer.energy + (self.power / self.efficiency) / 60 / 1000
                )

                if newEnergy <= self.outer.maxEnergy and newEnergy >= 0:
                    self.outer.energy = newEnergy
                else:
                    self.power = 0

                self.outer.hydrogentankLevel = (
                    self.outer.energy / self.outer.maxEnergy
                ) * 100


class Grid:
    def __init__(self, electricityPrice, feedInTariff, CO2perkWh):
        self.electricityPrice = electricityPrice
        self.feedInTariff = feedInTariff
        self.CO2perkWh = CO2perkWh
        self.energyDemand = 0
        self.energyFeedIn = 0

    def powerIn(self, P):

        # integrate power over time --> energy
        if P >= 0:
            self.energyFeedIn = self.energyFeedIn + (P / 60 / 1000)
        else:
            self.energyDemand = self.energyDemand + (P / 60 / 1000)


class Photovoltaic:
    # TODO Move some of these old parameters to the config file.
    def __init__(self, P_peak: int = None, PR: int = None, v: int = None,
            inclination: int = None, E_desired: int = None,
            config_file: Path = None):

        self.model = pv.default("PVWattsNone")

        # Loading the model parameters from the configuration file.
        with open(PVWATTS_CONFIG_PATH, 'r') as f:
            data = json.load(f)
            for key, val in data.items():
                if key != 'number_inputs' and key != '__':
                    self.model.value(key, val)

        self.model.value("solar_resource_file",
                         str(next(SCENARIO_DIR.joinpath('0_Scenario_Initialisation',
                             'Inputs', 'Weather').glob('*.csv'))))

        # Loading class model parameters
        if config_file is None:
            config_file = SCENARIO_DIR.joinpath('0_Scenario_Initialisation',
                'Inputs', 'Configs', 'pv.json')

        with open(config_file, 'r') as f:
            self.config = json.load(f)

        # Load provided parameters
        if P_peak is not None:
            self.model.value("system_capacity", P_peak / 1000)
        if v is not None:
            self.model.value("azimuth", 180 + v)
        if inclination is not None:
            self.model.value("tilt", inclination)
        if PR is not None:
            self.model.value("losses", 100 - PR * 100)

        self.model.execute()

        # Here, we initialise the model based on the average daily energy that
        # you would like to generate, based on a certain year's data.

        # FIXME TODO Allow option of specifyig fixed PV size, and calculating the
        # system capacity from that.

        # nominal_power [kW] = land_area [m^2] / 5.263 [m^2/kW] * GCR

        if E_desired is not None:

            # We are going to try and figure out the required system capacity,
            # based on the weather data.
            if P_peak is not None:
                raise ValueError("P_peak must not be specified when " +
                                 "E_desired is specified.")

            average_daily_energy = self.model.Outputs.annual_energy / 365

            if abs(average_daily_energy - E_desired) != 0:
                # Calculating the energy generated from a panel of 1 kW nominal
                # power. This value is a linear constant.
                daily_kWh_per_kw = self.model.Outputs.kwh_per_kw / 365

                # Dividing the desired energy generation by the linear constant
                # to get the required nominal power.
                system_capacity = E_desired / daily_kWh_per_kw
                self.model.value("system_capacity", system_capacity)

                self.model.execute()
        self.system_capacity = self.model.SystemDesign.system_capacity

    def get_results(self):
        return self.model.Outputs.ac  # AC output in watts

    def get_size(self, GCR=None):
        # GCR: Ground Coverage Ratio. In a roof installation, this is 1.
        # However, in an open rack system, this won't be 1, as there will be
        # self-shading from the sub-arrays.

        # SAM approximates the land area as:
        # land_area [m^2] = nominal_power [kW] * 5.263 [m^2/kW] / GCR

        if GCR is None:
            GCR = self.config['GCR']

        self.land_area = self.system_capacity * 5.263 / GCR
        return self.land_area

    def calc_costs(self):

        tcc = self.config['tcc_per_kW'] * self.system_capacity  # Total capital cost: EUR
        foc = self.config['foc_per_kW'] * self.system_capacity  # Fixed operating costs: EUR
        fcr = Accountant.present_value(1, 0.05, 1,
                                       self.config['system_lifetime']) ** -1
            # Fixed charge rate. (Percentage of initial investment that needs
            # to be paid annually.)
        aep = self.model.Outputs.annual_energy  # Annual energy production: kWh
        voc = 0  # Variable operating costs [EUR/kw]
        self.lcoe = (fcr * tcc + foc) / aep + voc

    def calc_capex(self,):

        self.capex = self.config['tcc_per_kW'] * self.system_capacity
        return self.capex

    def calc_opex(self,):

        self.opex = Accountant.present_value(
            A=self.config['foc_per_kW'] * self.system_capacity,  # Fixed operating costs: EUR
            i=0.08,
            f=1,
            t=self.config['system_lifetime'],
        )

        return self.opex


class System:

    class FCVs:

        def __init__(self, distance: pd.DataFrame):

            self.distance = distance
            self.vehicle_ids = distance.columns

            self.fcv_list: t.List[FuelcellVehicle] = \
                [FuelcellVehicle(distance[vehicle_id], vehicle_id) for
                 vehicle_id in distance.columns]

        def calc_energies(self):

            self.mean_energies = []
            self._energies = []
            for fcv in self.fcv_list:
                self._energies.append(fcv.calc_energies())
                self.mean_energies.append(fcv.mean_energy)

            self.energies = pd.concat(self._energies, keys=self.vehicle_ids,
                                      names=['vehicle_id', 'date'])

            self.energy = self.energies[['energy']].unstack(0)

            return self.energy

        def calc_capex(self,):

            self.capex = 0
            for fcv in self.fcv_list:
                self.capex += fcv.calc_capex()

            return self.capex

        def calc_opex(self,):

            self.opex = 0
            for fcv in self.fcv_list:
                self.opex += fcv.calc_opex()

            return self.opex

    def __init__(self, scenario_dir: Path, distance: pd.DataFrame,
                 config_file: Path = None):
        """ The system class, contains objects for the fleet of FCVs, and the
            Hydrogen production plants. The fleet consists of FCV objects with
            tank objects. The filling stations consist of tank, compressor and
            electrolyser objects. """

        # Create a population of FCVs, corresponding to the input files of the
        # scenario.

        global SCENARIO_DIR
        SCENARIO_DIR = scenario_dir

        global BATT_CONFIG_PATH
        BATT_CONFIG_PATH = scenario_dir.joinpath(
            '0_Scenario_Initialisation', 'Inputs',
            'Configs', 'battery_stateful.json')

        global PVWATTS_CONFIG_PATH
        PVWATTS_CONFIG_PATH = scenario_dir.joinpath(
            '0_Scenario_Initialisation', 'Inputs',
            'Configs', 'pvwattsv8.json')

        # Loading class model parameters
        if config_file is None:
            config_file = SCENARIO_DIR.joinpath('0_Scenario_Initialisation',
                'Inputs', 'Configs', 'system.json')

        with open(config_file, 'r') as f:
            self.config = json.load(f)

        self.fcvs = self.FCVs(distance)

        self.fcvs.calc_energies()

        self.compressor = Compressor(self.fcvs.energies['energy'].unstack(0))
        self.electrolyser = Electrolyser(self.fcvs.energies['energy'].unstack(0))

        self.compressor.calc_energy()
        self.electrolyser.calc_energy()
        self.energy: pd.Series = (self.electrolyser.energy +
                                  self.compressor.energy_cost)
        self.pv = Photovoltaic(E_desired=self.energy.sum(1).mean())

    def calc_costs(self):
        """Calculate the capital and operating expenses of the system"""
        self.capex = self.fcvs.calc_capex() + \
                     self.compressor.calc_capex() + \
                     self.electrolyser.calc_capex() + \
                     self.pv.calc_capex()

        # fuel_rate = 1  # €/kWh
            # Get this from the profitability that the fuel-station is trying to
            # achieve. XXX
        self.opex = self.fcvs.calc_opex() + \
                    self.compressor.calc_opex() + \
                    self.electrolyser.calc_opex(self.config) + \
                    self.pv.calc_opex()

    def print(self):
                 # Hydrogen energy
        stats = (f"{self.fcvs.energy.sum(1).mean():.2f} kWh of "
                 f"hydrogen used by the {len(self.fcvs.vehicle_ids)} FCV(s) "
                 "to travel an average of "
                 f"{self.fcvs.distance.mean(1).mean()} km \n\n"
                 # Electrical energy
                 f"{self.energy.sum(1).mean():.2f} kWh electricity is "
                 "required to produce this hydrogen.\n\n"
                 # PV stats
                 "The required roof area of PV is approx: "
                 f"{self.pv.get_size(GCR=1)} m^2.\n"
                 "The total open-rack land area of PV is approx: "
                 f"{self.pv.get_size()} m^2.\n"
                 f"The system capacity is: {self.pv.system_capacity} kW\n\n"
                 # Costs
                 "The average cost of PV energy (over a 20 year lifecycle) "
                 f"is: {self.pv.lcoe} EUR/kWh.\n"
                 f"The total system capital cost is {self.capex:.2f}.")
        print(stats)
        return stats

    def plot(self,):

        # plt.rc('font', family='serif', size=12)
        # plt.rc('text', usetex=True)

        # Plot 1
        fig1, ax1 = plt.subplots()
        fig1_title = 'hydrogen_requirements'
        ax1.boxplot(
            [self.energy[column].dropna() for column in
             self.energy.columns],
            sym='x'
        )
        ax1.set_xticklabels(self.fcvs.vehicle_ids, rotation=30)
        ax1.set_xlabel('FCV ID')
        ax1.set_ylabel('Energy (kWh)')
        ax1.set_title('Daily energy consumption of Hydrogen for various FCV taxis')
        fig1.tight_layout()

        # Plot 2

        fig2, ax2 = plt.subplots()
        fig2_title = 'cost_breakdown'
        data = np.array([[self.fcvs.capex, self.compressor.capex,
                          self.electrolyser.capex, self.pv.capex],
                         [self.fcvs.opex, self.compressor.opex,
                          self.electrolyser.opex, self.pv.opex]])

        def _pie_labels(pct, allvals):
            absolute = int(np.round(pct/100.*np.sum(allvals)))
            return "{:.1f}% (€{:0,.2f})".format(pct, absolute)

        size = 0.3
        cmap = plt.get_cmap('tab20c')
        outer_colors = cmap(np.arange(3)*4)
        inner_colors = cmap([1, 2, 3, 1, 5, 6, 7, 5])

        _plot_data = data.sum(axis=1)
        ax2.pie(_plot_data, labels=['Capex', 'Opex'],
                radius=1, colors=outer_colors,
                wedgeprops=dict(width=size, edgecolor='w'),
                autopct=lambda pct: _pie_labels(pct, _plot_data),
                pctdistance=1 - size/2,
                textprops=dict(ha='center'),
                )
        _plot_data  = data.flatten()
        ax2.pie(_plot_data,
                labels=['FCVs', 'Compressor', 'Electrolyser', 'Photovoltaics',
                        'FCVs', 'Compressor', 'Electrolyser', 'Photovoltaics'],
                labeldistance=1-size/2,
                textprops=dict(ha='center'),
                radius=1 - size,
                colors=inner_colors,
                wedgeprops=dict(width=size, edgecolor='w'),
                # autopct=lambda pct: _pie_labels(pct, _plot_data),
                autopct='%1.1f%%',
                )
        fig2.tight_layout()

        return [(fig1, fig1_title), (fig2, fig2_title)]
