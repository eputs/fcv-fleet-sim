from pathlib import Path


def _generate_configs():
    ...


def _sim_configs():
    ...


def _choose_best_result():
    ...


def optimise_system(scenario_dir: Path, **kwargs):

    _generate_configs()

    _sim_configs()

    _choose_best_result()

    ...
