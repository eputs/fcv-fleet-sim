import argparse
from pathlib import Path
import pandas as pd
from haversine import haversine
from tqdm import tqdm


def calc_distances(scenario_dir: Path) -> pd.DataFrame:

    gps_files = sorted([*scenario_dir.joinpath('1_Data_Preprocessing',
                       'Filtered_Data').glob('*.csv')])

    for gps_file in tqdm(gps_files):

        gps_trace = pd.read_csv(gps_file)

        # Using haversine to get distance
        prev_date = None
        distances = {}
        for _, datapoint in gps_trace.iterrows():
            date = datapoint['Time'].split(' ')[0]
            coordinate = (datapoint['Latitude'], datapoint['Longitude'])
            if date != prev_date:
                distances[date] = 0
                prev_date = date
                prev_coordinate = coordinate
                continue
            distances[date] += haversine(prev_coordinate, coordinate, unit='m')
            prev_date = date
            prev_coordinate = coordinate

        distances_df = pd.DataFrame({'Date': distances.keys(),
                                     'Distance (m)': distances.values()})

        distances_df.to_csv(
            scenario_dir.joinpath('1_Data_Preprocessing', 'Distances',
                f'{gps_file.stem}.csv'), index=False)
