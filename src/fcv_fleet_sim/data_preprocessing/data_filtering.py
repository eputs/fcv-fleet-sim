"""
Filters GPS data-set spatially and temporally.

Spatial filtering is done according to a geographical area, specified in the
boundary.csv file. The temporal filtering, filters out weekend data, weekday
data, or nothing.

For the spatial filtering, datapoints outside of the geographical area are
identified. Days in which these datapoints are discovered, are discarded from
the dataset. Weekends are (optionally) discarded.

Inputs:
    Taxi trace data for an extended time period (CSV).
        Preferably it should already have gone thought the clustering script.
    Geographical boundary (Geojson)
Outputs:
    Filtered GPS datapoints (CSV)
    Statistics (txt) of:  # TODO
        Dates removed
        Percentage of datapoints found outside of boundary

Author: Chris Abraham
Date: 2022-07-08
"""

# %% Imports ##################################################################
from pathlib import Path
import pandas as pd
from tqdm import tqdm
import datetime as dt
import data_processing_ev as dpr
import itertools
from multiprocessing import Pool, RLock, cpu_count
from operator import itemgetter
import typing as t


FILTERS = {'weekends': 0, 'weekdays': 1, 'none': 2}


# %% Functions ################################################################
def is_point_in_poly(x: int, y: int, poly) -> bool:
    """
    Determine if the point is in the path.

    The algorithm uses the ["Even-Odd Rule"](https://en.wikipedia.org/wiki/
    Even–odd_rule)

    Args:
      x -- The x coordinates of point.
      y -- The y coordinates of point.
      poly -- a list of tuples [(x, y), (x, y), ...]

    Returns:
      True if the point is in the path.
    """
    num = len(poly)
    i = 0
    j = num - 1
    c = False
    for i in range(num):
        if ((poly[i][1] > y) != (poly[j][1] > y)) and \
                (x < poly[i][0] + (poly[j][0] - poly[i][0])
                                  * (y - poly[i][1])  # noqa
                                  / (poly[j][1] - poly[i][1])):  # noqa
            c = not c
        j = i
    return c


def filter_trace(input_file: Path, output_path: Path,
                 filter_type: FILTERS, **kwargs):

    if filter_type == FILTERS['none']:
        def filter(row_vals: t.List):
            return True
    elif filter_type == FILTERS['weekends']:
        def filter(row_vals: t.List):
            day_of_week = dt.datetime.fromisoformat(row_vals[1]).weekday()
            return day_of_week < 5
    elif filter_type == FILTERS['weekdays']:
        def filter(row_vals: t.List):
            day_of_week = dt.datetime.fromisoformat(row_vals[1]).weekday()
            return day_of_week >= 5

    output_file = output_path.joinpath(f'{input_file.stem}.csv')
    # Write the header row.
    header_row = ("GPSID,Time,Latitude,Longitude,Altitude," +
                  "Heading,Satellites,HDOP,AgeOfReading,"   +
                  "DistanceSinceReading,Velocity")
    output_file.parent.mkdir(parents=True, exist_ok=True)
    with open(output_file, 'w') as f_out:
        f_out.write(header_row + '\n')
    # For each file in files_of_vehicle:
    new_rows = []
    with open(input_file, 'r') as f_in:
        # Read and discard the header row.
        f_in.readline()
        # For each remaining row in the file:
        for row in f_in:
            # Re-format the row.
            row_vals = row[:-1].split(',')
            if filter(row_vals):
                new_row_vals = []
                new_row_vals.append(row_vals[0])  # GPSID
                new_row_vals.append(row_vals[1])  # Time
                new_row_vals.append(row_vals[2])  # Latitude
                new_row_vals.append(row_vals[3])  # Longitude
                new_row_vals.append(row_vals[4])  # Altitude
                new_row_vals.append(row_vals[5])  # Heading
                new_row_vals.append(row_vals[6])  # Satellites
                new_row_vals.append(row_vals[7])  # HDOP
                new_row_vals.append(row_vals[8])  # AgeOfReading
                new_row_vals.append(row_vals[9])  # DistanceSinceReading (km)
                new_row_vals.append(row_vals[10])  # Velocity (mph -> km/h)
                new_rows.append(new_row_vals)

    # Write the rows in the vehicle's output file.
    new_rows = sorted(new_rows, key=itemgetter(1))  # Sort rows by time.
    new_rows = [','.join(row) + '\n' for row in new_rows]
    with open(output_file, 'a') as f_out:
        # f_out.write(new_rows + '\n')
        f_out.writelines(new_rows)


# %% Main #####################################################################
def filter_scenario(scenario_dir: Path, **kwargs):
    """
    Take clustered traces and filter out clusters outside of map boundary.

    Export the resulting dataframe as a csv file.
    """

    autorun = kwargs.get('autorun', False)
    # TODO Make option of discarding weekends a command-line argument.

    input_files = sorted([*scenario_dir.joinpath(
        '0_Scenario_Initialisation', 'Inputs', 'Traces').glob('*.csv')])
    output_path = scenario_dir.joinpath('1_Data_Preprocessing', 'Filtered_Data')

    # Check if files exist in output_path.
    if any(output_path.glob('*/*.csv')):
        print(f"Warning: Files exist in {output_path}.\n\t" +
              "You may want to delete them!")
        if not autorun:
            input("Press any key to continue...")

    if not autorun:
        _ = input("What part of the data would you like to filter out? " +
                  "(weekends/weekdays/[none])  ")
        filter_type = FILTERS['none'] if _ == "" else FILTERS[_.lower()]
    else:
        filter_type = FILTERS['none']

    # IF DEBUGGING:
    # -------------
    for input_file in tqdm(input_files):
        filter_trace(input_file, output_path, filter_type, **kwargs)
