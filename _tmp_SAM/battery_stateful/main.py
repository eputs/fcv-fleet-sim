#!/usr/bin/env python3

import PySAM.BatteryStateful as bs
from pathlib import Path
import json
import logging as log


class Battery:

    def __init__(
            self, batt_config_path: Path, systemCost: float,
            CO2_KgperkWh: float, maxPower=None, maxEnergy=None,
            energy=None, efficiency=None):

        self.model = bs.new()
        with open(batt_config_path, 'r') as config_file:
            data = json.load(config_file)
            for key, val in data.items():
                if key != 'number_inputs' and key != '__':
                    self.model.value(key, val)

        if maxPower is not None:
            self.model.value('input_power', maxPower)
        if maxEnergy is not None:
            self.model.value('nominal_energy', maxEnergy)
        if energy is not None:
            self.model.value('initial_SOC',
                             energy / self.model.value('nominal_energy') * 100)
        if efficiency is not None:
            log.error('Efficiency is deprecated. Use more detailed battery ' +
                      'modelling parameters.')
            input('Press enter to ignore the efficiency parameter and ' +
                  'continue... Else, press <Ctrl> + C to quit the program.')

        self.systemCost = systemCost
        self.CO2_KgperkWh = CO2_KgperkWh

        self.model.setup()

    def step(self):
        self.model.execute()


if __name__ == "__main__":

    print("""
                                  _\|/_
                                  (o o)
                          +----oOO-{_}-OOo-+
                          |  Model Tester  |
                          +----------------+
This program will allow you to test the battery_stateful model. Simply press
enter to continue, and then enter the "n" command to iteratively run the
simulation of the battery.
""")

    input("Press enter to continue...")

    my_batt = Battery(
        Path(
            __file__
            ).parent.joinpath('battery_stateful.json'),
        50,
        150
    )

    while True:
        breakpoint()  # XXX
        my_batt.step()
