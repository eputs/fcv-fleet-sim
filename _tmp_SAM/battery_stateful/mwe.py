# Import libraries:
from PySAM import BatteryStateful
import json
from time import sleep

# Initialise the model:
model = BatteryStateful.new()
with open("mwe.json.txt", 'r') as parameter_file:
    data = json.load(parameter_file)
    for key, val in data.items():
        model.value(key, val)
model.value('dt_hr', 1 / 60)  # Run the simulation for a one-minute timestep.
P_in = -10  # Let the power input be P_in, in kW.
model.value('input_power', P_in)
model.setup()

while True:

    valid_input = False
    while not valid_input:
        try:
            P_in = float(input("Enter the power *drawn* from the battery " +
                               "[kW] (Negative value charges the battery): "))
            valid_input = True
        except Exception:
            print("Invalid input. Try again...")
            sleep(1)

    model.value('input_power', P_in)

    original_SOC = model.StatePack.SOC

    # Run the model for one timestep, with an input power of P_in:
    model.execute()  # Execute the model.

    # View the result:
    print("Power provided to battery: \n\t" + str(-P_in) + " kW")
    print("Power absorbed by battery: \n\t" + str(abs(model.StatePack.P)) + " kW")
    print("Expected SOC of battery: \n\t" +
          str((-P_in * 1 / 60) / model.ParamsPack.nominal_energy * 100 +
              original_SOC) + " %")
    print("Actual SOC of battery: \n\t" + str(model.StatePack.SOC) + " %")
    # print("Energy actually absorbed by the battery (from SOC): \n\t" +
    #       str(model.StatePack.SOC / 100 * model.ParamsPack.nominal_energy) + " kWh")
    # print("Energy actually absorbed by the battery (from Q and V): \n\t" +
    #       str(model.StatePack.Q * model.StatePack.V / 1000) + " kWh")
    # print("Energy actually absorbed by the battery (from I and V): \n\t" +
    #       str(-model.StatePack.I * model.StatePack.V * 1 / 60 / 1000) + " kWh")
    # print("Current into the battery (I): \n\t" +
    #       str(-model.StatePack.I) + " A")
    print()
