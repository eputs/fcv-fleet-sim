#!/usr/bin/env python3

# General imports
import sys
from pathlib import Path
import matplotlib.pyplot as plt
import json
import numpy as np
import datetime as dt
import itertools
import pickle

# SAM imports
from PySAM import Pvwattsv8

# ElySim imports
sys.path.insert(0, '/home/abrac/Documents/Masters/Coding/Projects/elysimulation/Masterprojekt_WiSe2021/code')
import parameter_base_case as pbc
import photovoltaik as pv

minutes = np.arange(
    dt.datetime(2019, 1, 1),
    dt.datetime(2020, 1, 1),
    dt.timedelta(minutes=1),
).astype(dt.datetime)

# Generate ElySim PV output.
P_out_elysim = pv.calculate_power(pbc.G_h, pbc.z, pbc.P_peak, pbc.PR, pbc.G_stc, pbc.v, pbc.inclination)

# Generate SAM pv output.
pv_sam_model = Pvwattsv8.new()
PV_CONFIG_PATH = Path(__file__).parent.joinpath('pvwattsv8.json').absolute()
with open(PV_CONFIG_PATH, 'r') as config_file:
    data = json.load(config_file)
    for key, val in data.items():
        if key != 'number_inputs' and key != '__':
            pv_sam_model.value(key, val)
# pv_sam_model.value("solar_resource_file", "/home/abrac/SAM Downloaded Weather Files/berlin_msg-iodc_15_2019.csv")
pv_sam_model.execute()
P_in_sam = pv_sam_model.Outputs.gh
P_in_sam = [power for power in P_in_sam for i in
            range(int(525600/len(P_in_sam)))]
P_out_sam = pv_sam_model.Outputs.ac
P_out_sam = [power for power in P_out_sam for i in
            range(int(525600/len(P_out_sam)))]

# Print outputs!
# print(f"POA: \n\t{model.Outputs.poa}\n\n")
# print(f"POA Monthly: \n\t{model.Outputs.poa_monthly}\n\n")


# Plot ElySim PV input:
plt.figure(0)
plt.suptitle("Model input data: Glolbal horizontal irradiance")
ax0_1 = plt.subplot(2, 1, 1)
ax0_1.set_ylabel('Gₕ irradiance (W/m²)')
ax0_1.set_title('ElySim Data')
ax0_1.plot(minutes, np.concatenate([pbc.G_h, [0]*60]))

# Plot ElySim PV output:
plt.figure(1)
plt.suptitle("Model output data: AC power output")
ax1_1 = plt.subplot(2, 1, 1)
ax1_1.set_ylabel('PV output (W)')
ax1_1.set_title('ElySim Model')
# ax1_1.plot(minutes, P_out_elysim)

# Plot SAM PV input:
plt.figure(0)
ax0_2 = plt.subplot(2, 1, 2, sharex=ax0_1, sharey=ax0_1)
ax0_2.set_ylabel('Gₕ irradiance (W/m²)')
ax0_2.set_title('SAM Data')
ax0_2.plot(minutes, P_in_sam)
plt.tight_layout()

# Plot SAM PV output:
plt.figure(1)
ax1_2 = plt.subplot(2, 1, 2, sharex=ax1_1, sharey=ax1_1)
ax1_2.set_ylabel('PV output (W)')
ax1_2.set_xlabel('Time')
ax1_2.set_title('SAM Model')
ax1_2.plot(minutes, P_out_sam)
plt.tight_layout()

filename = f"input_{dt.datetime.now().strftime('%Y_%m_%d_%H_%M_%S')}.fig.pickle"
pickle.dump(plt.figure(0), open(filename, 'wb'))

filename = f"output_{dt.datetime.now().strftime('%Y_%m_%d_%H_%M_%S')}.fig.pickle"
pickle.dump(plt.figure(1), open(filename, 'wb'))

plt.show()
