#!/usr/bin/env python3

import sys
sys.path.append("../../Masterprojekt_WiSe2021/code/")
import Simu_Klassen as sk
import time
from pathlib import Path
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
from tqdm import tqdm

if __name__ == "__main__":

    distance_files = sorted([*Path(__file__).parents[1].joinpath(
        '1_Distance_calculator', 'dists').glob('*.csv')])
    vehicle_ids = [distance_file.stem for distance_file in distance_files]

    daily_fcv_energies = []
    daily_system_energies = []

    _ = input("Would you like to run in interactive mode? y/[n]  ")
    interactive_mode = False if _.lower() != 'y' else True

    distances_list = []

    for distance_file in distance_files:

        distances = pd.read_csv(distance_file)
        distances['Date'] = pd.to_datetime(distances['Date'])
        distances = distances.set_index('Date')
        distances = distances['Distance (m)']
        distances.name = 'distance'
        distances /= 1000
        distances_list.append(distances)

    system = sk.System(pd.concat(distances_list, axis=1, keys=vehicle_ids))

    fcv_energy: pd.DataFrame = system.fcvs.energies['energy'].unstack(0)

    system.pv.calc_costs()

    system.calc_costs()

    ###########
    # Outputs #
    ###########

    system.print()
    
    # TODO Add the below into system.print()

    # Print the land area required for this pv installation.
    print("The required roof area of PV is approx: " + 
          f"{system.pv.get_size()} m^2.")
    print("The total open-rack land area of PV is approx: " + 
          f"{system.pv.get_size()/0.3} m^2.")
    print(f"The system capacity is: {system.pv.system_capacity} kW")

    print("The average cost of PV energy (over a 20 year lifecycle) is: " +
          f"{system.pv.lcoe} EUR/kWh.")

    print(f"The total system capital cost is {system.capex}.")  # TODO formatting

    # Plotting results
    plt.boxplot(
        [system.energy[column].dropna() for column in 
            system.energy.columns],
        sym='x'
    )
    plt.gca().set_xticklabels(vehicle_ids, rotation=30)
    plt.xlabel('FCV ID')
    plt.ylabel('Energy (kWh)')
    plt.title('Daily energy consumption of Hydrogen for various FCV taxis')
    plt.tight_layout()
    plt.show()
